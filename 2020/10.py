import time, re, copy
from functools import reduce, lru_cache
import aoctimer

rows = open('2020/10.txt').read().split('\n')
rows = sorted([int(x) for x in rows])
rows = [0] + rows + [rows[-1]+3]

diffs = [rows[i+1] - rows[i] for i in range(0, len(rows) - 1)]
print(sum([x==1 for x in diffs]) * sum([x==3 for x in diffs]))

@lru_cache #without this, takes thousands of years
def valid_routes_from(i):
    res = 0
    if i == len(rows) - 1:
        res = 1
    else:
        k = i+1
        while k<len(rows) and rows[k]-rows[i] <=3:
            res += valid_routes_from(k)
            k += 1
    return res

print(valid_routes_from(0))
