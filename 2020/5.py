import time, re
from functools import reduce
import aoctimer

data = open('2020/5.txt').read()
rows = data.split('\n')

seats = set()
for row in rows:
    back = 0
    right = 0
    for i in range(7):
        back += pow(2, 6-i) * (row[i]=='B')
    for i in range(3):
        right += pow(2, 2-i) * (row[7+i]=='R')
    seat = 8*back + right
    seats.add(seat)

print(max(seats))
print([x+1 for x in seats if x+1 not in seats and x+2 in seats])
