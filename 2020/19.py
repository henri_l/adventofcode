import time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

data = [g.split('\n') for g in open('2020/19.txt').read().split('\n\n')]

rules = {}
for row in data[0]:
    [(id, rest)] = re.findall('(\d+): (.*)', row)
    for rulestr in rest.split('|'):
        rule = []
        for sub in re.findall('"(\w+)"|(\d+)', rulestr):
            rule.append(sub[0] if sub[0] else int(sub[1]))
        rules.setdefault(int(id), []).append(rule)

msgs = data[1]

part_two = True
if part_two:
    rules[8] = [[42], [42, 8]]
    rules[11] = [[42, 31], [42, 11, 31]]

def collect(id, msg, start_i=0):
    res = set()
    for rule in rules[id]:
        ixs = {start_i}
        for sub in rule:
            tmp = set()
            for ix in ixs:
                if isinstance(sub, int):
                    tmp |= collect(sub, msg, ix)
                elif msg[ix:ix+len(sub)] == sub:
                    tmp.add(ix+len(sub))
            ixs = tmp
        res |= ixs
    return res

def ismatch(msg):
    return len(msg) in collect(0, msg)

print(sum(map(ismatch, msgs)))
