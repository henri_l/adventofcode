import time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

groups = [g.split('\n') for g in open('2020/20.txt').read().split('\n\n')]

n_square = int(math.isqrt(len(groups)))
n_side = len(groups[0][1])

blocks = {}
full_squares = {}
for group in groups:
    [(id)] = re.findall('Tile (\d+):', group[0])
    grid = group[1:]
    full_squares[int(id)] = grid
    sides = [
        grid[0][::-1],
        ''.join(grid[i][0] for i in range(len(grid))),
        grid[n_side-1],
        ''.join(grid[i][n_side-1] for i in range(len(grid)-1,-1,-1)),
    ]
    blocks[int(id)] = sides

# 1) flip: flip by y-axis
# 2) orientation: x*90 degrees clockwise
def rotated_ix(x, y, orientation, flip):
    for _ in range(orientation):
        x, y = y, n_side-1-x
    if flip:
        x, y = n_side-1-x, y
    return x, y

# side of rotated square by side index
def get_side(spec, side):
    (_, block, orientation, flip) = spec
    block_ixs = [0, 3, 2, 1] if flip else [0, 1, 2, 3]
    s = block[block_ixs[(side+orientation)%4]]
    return s[::-1] if flip else s

def side_match(spec_left, spec_right):
    left = get_side(spec_left, 3)
    right = get_side(spec_right, 1)
    return left[::-1] == right

def above_match(spec_top, spec_bottom):
    top = get_side(spec_top, 2)
    bottom = get_side(spec_bottom, 0)
    return top[::-1] == bottom

def create_full_grid(specs): # for part two
    res = []
    n_side_no_borders = n_side-2
    for yy in range(n_square*n_side_no_borders):
        ys = yy//n_side_no_borders
        y = yy%n_side_no_borders + 1
        row = ''
        for xx in range(n_square*n_side_no_borders):
            xs = xx//n_side_no_borders
            x = xx%n_side_no_borders + 1
            spec = specs[(xs, ys)]
            [id, _, orientation, flip] = spec
            x2, y2 = rotated_ix(x, y, orientation, flip)
            row += full_squares[id][y2][x2]
        res.append(row)
    return res

full_matches = [] # part two

def recursive_find(placed, free):
    row_ix = len(placed) // n_square
    col_ix = len(placed) % n_square
    for id in list(free):
        block = blocks[id]
        for orientation in range(4):
            for flip in (True, False):
                spec = (id, block, orientation, flip)
                match = True
                if row_ix > 0:
                    match = match and above_match(placed[(col_ix, row_ix-1)], spec)
                if col_ix > 0:
                    match = match and side_match(placed[(col_ix-1, row_ix)], spec)
                if match:
                    placed[(col_ix, row_ix)] = spec
                    free.remove(id)
                    if free:
                        recursive_find(placed, free)
                    else:
                        e = n_square-1
                        print("found match: {}".format([placed[x][0] for x in ((0,0), (0,e), (e,0), (e,e))]))
                        full_matches.append(create_full_grid(placed))
                    placed.pop((col_ix, row_ix))
                    free.add(id)

recursive_find({}, set(blocks.keys()))

# part two

def is_monster(grid, x, y):
    for dx, dy in [
        (0, 1), (1, 2),
        (4, 2), (5, 1), (6, 1), (7, 2),
        (10, 2), (11, 1), (12, 1), (13, 2),
        (16, 2), (17, 1), (18, 0), (18, 1), (19, 1),
    ]:
        if grid[y+dy][x+dx] != '#':
            return False
    return True

for pic in full_matches:
    count = 0
    for y in range(len(pic)-2):
        for x in range(len(pic[0])-19):
            if is_monster(pic, x, y):
                count += 1
    if count > 0:
        total_hashes = sum(r.count('#') for r in pic)
        print(total_hashes - count*15)
