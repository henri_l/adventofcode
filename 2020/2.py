import time, re
from functools import reduce
import aoctimer

data = open('2020/2.txt').read()
rows = data.split('\n')

x = 0
for row in rows:
    (a, b, c, pw) = re.findall('(.*)-(.*) (.*): (.*)', row)[0]
    a = int(a)
    b = int(b)
    q = 0
    q += (len(pw) >= a and pw[a-1] == c)
    q += (len(pw) >= b and pw[b-1] == c)
    if q == 1:
        x += 1
print(x)
