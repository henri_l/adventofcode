import time, re, copy
from functools import reduce, lru_cache
import aoctimer

def next(arr, i, j):
    cur = arr[i][j]
    if cur in ['L', '#']:
        acc_count = 0
        for di in [-1, 0, 1]:
            for dj in [-1, 0, 1]:
                if (di!=0 or dj!=0):
                    dist = 1
                    while arr[i+dist*di][j+dist*dj] == '.':
                        dist += 1
                    if arr[i+dist*di][j+dist*dj] == '#':
                        acc_count += 1
        if cur == 'L' and acc_count == 0:
            return '#'
        elif cur == '#' and acc_count >= 5:
            return 'L'
    return cur

rows = open('2020/11.txt').read().split('\n')
prev = []
while rows != prev:
    prev = copy.deepcopy(rows)
    for i in range(len(rows)):
        newrow = ''
        for j in range(len(rows[0])):
            newrow += next(prev, i, j)
        rows[i] = newrow

print(sum([x.count('#') for x in rows]))
