import time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

rows = open('2020/13.txt').read().split('\n')
earliest = int(rows[0])
schedule = rows[1].split(',')
bs = [int(x) for x in schedule if x != 'x']

t = earliest

best_t = 10000
best_b = 0

for b in bs:
    past = earliest - b
    left = (b - past) % b
    if left < best_t:
        best_t = left
        best_b = b
    print(b, left)
print(best_b*best_t)

#========================================

def lcm(a, b):
    return (a*b) // math.gcd(a, b)

def find(cycle, t, buses):
    (bus, delay) = buses[0]
    while (t+delay)%bus != 0:
        t += cycle
    if len(buses) == 1:
        return t
    cycle2 = lcm(cycle, bus)
    print(t, cycle2)
    return find(cycle2, t-cycle2, buses[1:])

bs = [(int(schedule[i]), i) for i in range(len(schedule)) if schedule[i]!='x']

first = bs[0][0]
print('')
print(find(first, first, bs))
