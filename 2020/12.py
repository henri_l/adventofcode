import time, re, copy
from functools import reduce, lru_cache
import aoctimer

rows = open('2020/12.txt').read().split('\n')
insts = [(x[0], int(x[1:])) for x in rows]

def turn(c, dir):
    if c=='R':
        return (dir[1], -dir[0])
    elif c=='L':
        return (-dir[1], dir[0])
    raise RuntimeError("bad turn " + c)

part_two = True

pos = (10, 1) if part_two else (0, 0)
pos2 = (0, 0)
dir = (1, 0)

for (inst, n) in insts:
    if inst in ['E', 'W', 'N', 'S']:
        pos = (
            pos[0] + (n if inst=='E' else -n if inst == 'W' else 0),
            pos[1] + (n if inst=='N' else -n if inst == 'S' else 0),)
    elif inst in ['L', 'R', 'F', 'R']:
        if part_two:
            if inst in ['L', 'R']:
                for t in range(n//90):
                    pos = turn(inst, pos)
            elif inst == 'F':
                for _ in range(n):
                    pos2 = (pos2[0]+pos[0], pos2[1]+pos[1])
            elif inst == 'R':
                for _ in range(n):
                    pos2 = (pos2[0]-pos[0], pos2[1]-pos[1])
        else:
            if inst in ['L', 'R']:
                for _ in range(n//90):
                    dir = turn(inst, dir)
            elif inst == 'F':
                pos = (pos[0]+n*dir[0], pos[1]+n*dir[1])
            elif inst == 'R':
                pos = (pos[0]-n*dir[0], pos[1]-n*dir[1])
    else:
        raise RuntimeError("bad inst " + inst)

if part_two:
    print(pos2)
    print(abs(pos2[0])+abs(pos2[1]))
else:
    print(pos)
    print(abs(pos[0])+abs(pos[1]))
