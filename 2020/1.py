import time
from functools import reduce
import aoctimer

data = open('2020/1.txt').read()
rows = data.split('\n')

arr = [int(r) for r in rows]
arr.sort()

print(arr)

def find_sums(arr, a, b, requiredSum):
    res = []
    while a<b:
        s = arr[a] + arr[b]
        if s < requiredSum:
            a += 1
        elif s > requiredSum:
            b -= 1
        elif s == requiredSum:
            res += [arr[a],arr[b]]
            a += 1
            b -= 1
    return res

mult = lambda x,y : x*y

res = find_sums(arr, 0, len(arr)-1, 2020)
print(reduce(mult, res))

res2 = []
for i in range(0, len(arr)-2):
    first = arr[i]
    requiredSum = 2020 - first
    tmpRes = find_sums(arr, i+1, len(arr)-1, requiredSum)
    if tmpRes:
        res2 += [first]*(len(tmpRes)//2) + tmpRes
print(res2)
print(reduce(mult, res2))

#========================================
# brute force version

#START = time.time()
#res = []
#for a in range(0, len(arr)-2):
#    for b in range(a+1, len(arr)-1):
#        for c in range(b+1, len(arr)):
#            if arr[a]+arr[b]+arr[c] == 2020:
#                res += [arr[a],arr[b],arr[c]]
#print(res)
#print(reduce(mult, res2))
