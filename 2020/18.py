import time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

data = open('2020/18.txt').read().split('\n')

def parseVal(f, i):
    if f[i] == '(':
        val, i = calc(f, i+1)
        return val, i+1
    else:
        numstr = ''
        while i < len(f) and f[i].isnumeric():
            numstr += f[i]
            i += 1
        return int(numstr), i

op_def = {
    '+' : [2, lambda x,y: x+y],
    '*' : [1, lambda x,y: x*y],
}

def getOp(f, i):
    [prio, op] = op_def[f[i]]
    return op, prio

def calc(f, i = 0, prio = 0):
    res, i = parseVal(f, i)
    while i<len(f):
        if f[i] in op_def.keys():
            op, op_prio = getOp(f, i)
            if op_prio < prio:
                break
            else:
                i += 1
                val2, i = calc(f, i, op_prio)
                res = op(res, val2)
        elif f[i] == ')':
            break
    return res, i

print(sum([calc(h)[0] for h in data]))
