import time, re
from functools import reduce
import aoctimer

data = open('2020/4.txt').read()
rows = data.split('\n')
pp = dict()
valid = 0
for row in rows:
    if len(row)==0:
        valid += all(x in pp.keys() for x in ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'])
        pp.clear()
    else:
        (k, f) = re.findall(r'(.*):(.*)', row)[0]
        def hgt_rule(f):
            r = re.findall(r'(\d+)(cm|in)', f)
            return r and ((r[0][1] == 'cm' and 150 <= int(r[0][0]) <= 193)
                       or (r[0][1] == 'in' and 59 <= int(r[0][0]) <= 76))
        rules = {
            'byr' : (lambda f : re.match(r'\d\d\d\d', f) and 1920 <= int(f) <= 2002),
            'iyr' : (lambda f : re.match(r'\d\d\d\d', f) and 2010 <= int(f) <= 2020),
            'eyr' : (lambda f : re.match(r'\d\d\d\d', f) and 2020 <= int(f) <= 2030),
            'hgt' : hgt_rule,
            'hcl' : (lambda f : len(f) == 7 and re.match(r'#[a-f0-9]*', f)),
            'ecl' : (lambda f : f in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']),
            'pid' : (lambda f : len(f) == 9 and re.match(r'[0-9]*', f)),
        }
        if k in rules and rules[k](f):
            pp[k] = f
print(valid)
