import time, re
from functools import reduce
import aoctimer

data = open('2020/6.txt').read()
groups = [g.split('\n') for g in data.split('\n\n')]

print(sum([len(reduce(set.union, [set(x) for x in g])) for g in groups]))
print(sum([len(reduce(set.intersection, [set(x) for x in g])) for g in groups]))
