import time, re, copy, math, operator
from functools import reduce, lru_cache
import aoctimer
from aocutils import Point

rows = open('2020/24.txt').read().split('\n')

paths = []

# unit vectors x = e, y = ne
dirs = {
    'w':  Point(-1, 0),
    'e':  Point(1, 0),
    'nw': Point(-1, 1),
    'ne': Point(0, 1),
    'sw': Point(0, -1),
    'se': Point(1, -1),
}
for row in rows:
    path = []
    i = 0
    while i < len(row):
        for compass, dir in dirs.items():
            if row.startswith(compass, i):
                path.append(dir)
                i += len(compass)
    paths.append(path)

blacks = set()
for path in paths:
    endpos = reduce(lambda a,b: a+b, path)
    blacks.symmetric_difference_update({endpos}) # add or remove

print(len(blacks))

# part two

def neighbours(c):
    return set(c+dir for dir in dirs.values())

for _ in range(100):
    check = blacks.copy()
    for c in blacks:
        check.update(neighbours(c))
    blacks2 = set()
    for c in check:
        bn = len(blacks.intersection(neighbours(c)))
        if (c in blacks and bn in (1, 2)) or (c not in blacks and bn == 2):
            blacks2.add(c)
    blacks = blacks2

print(len(blacks))
