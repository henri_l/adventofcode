import time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

data = open('2020/17.txt').read().split('\n')

part_two = True

state = set(
    (x, y, 0, 0)
    for y in range(len(data))
    for x in range(len(data[0]))
    if data[y][x] == '#')

def neighbours(loc):
    return set(
        (loc[0]+dx, loc[1]+dy, loc[2]+dz, loc[3]+dw)
        for dx in (-1, 0, 1)
        for dy in (-1, 0, 1)
        for dz in (-1, 0, 1)
        for dw in ((-1, 0, 1) if part_two else (0,))
        if any((dx, dy, dz, dw)))

def alive(loc):
    active = len(neighbours(loc).intersection(state))
    return active in ((2, 3) if loc in state else (3,))

for _ in range(6):
    check = state.union(*map(neighbours, state))
    state = set(filter(alive, check))

print(len(state))
