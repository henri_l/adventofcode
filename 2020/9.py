import time, re, copy
from functools import reduce
import aoctimer

rows = open('2020/9.txt').read().split('\n')
rows = [int(x) for x in rows]

def includesSum(numbers, requiredSum):
    arr = sorted(numbers)
    a = 0
    b = len(arr)-1
    while a<b:
        s = arr[a] + arr[b]
        if s < requiredSum:
            a += 1
        elif s > requiredSum:
            b -= 1
        elif s == requiredSum:
            return True
    return False

n = 25
for i in range(n, len(rows)):
    if not includesSum(rows[i-n:i], rows[i]):
        print(i, rows[i])

nb = 22477624
a = 0
b = 1
while a < len(rows):
    s = sum(rows[a:b+1])
    if s < nb:
        b += 1
    elif s > nb:
        a += 1
    else:
        print(a, b, min(rows[a:b+1]) + max(rows[a:b+1]))
        break
