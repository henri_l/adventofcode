import time, re
from functools import reduce
import aoctimer

rows = open('2020/7.txt').read().split('\n')
rules = {}
for row in rows:
    [(outcolor, rest)] = re.findall(r'(\w+ \w+) bags contain (.*)', row)
    rules[outcolor] = re.findall(r'(\d+) (\w+ \w+) bag', rest)

def contains(outcolor, incolor, rules):
    return any(map(lambda x: x[1]==incolor or contains(x[1], incolor, rules), rules[outcolor]))

def countbags(color, rules):
    return sum(map(lambda x: int(x[0])*(1+countbags(x[1], rules)), rules[color]))

print(sum(map(lambda x: contains(x, 'shiny gold', rules), rules.keys())))
print(countbags('shiny gold', rules))
