import time, re
from functools import reduce
import aoctimer

data = open('2020/3.txt').read()
rows = data.split('\n')

m = len(rows[0])
x = 1
for [i_inc, j_inc] in [[1,1],[1,3],[1,5],[1,7],[2,1],]:
    i=i_inc
    j=j_inc
    count=0
    while i < len(rows):
        if rows[i][j] == '#':
            count += 1
        i += i_inc
        j = (j+j_inc)%m
    x*=count
print(x)
