import time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

rows = open('2020/21.txt').read().split('\n')

allgPoss = {} # allergen -> possible ingredients
ingsConcat = [] # all ingredient lists concatenated
for row in rows:
    [(ingStr, allgStr)] = re.findall('(.*) \(contains (.*)\)', row)
    ingslist = ingStr.split(' ')
    ingsConcat += ingslist
    ings = set(ingslist)
    allgs = allgStr.split(', ')
    for allg in allgs:
        allgPoss.setdefault(allg, ings.copy()).intersection_update(ings)

while max(len(x) for x in allgPoss.values()) > 1:
    for allg, ings in allgPoss.items():
        if len(ings) == 1:
            for allg2, ings2 in allgPoss.items():
                if allg2 != allg:
                    ings2 -= ings

nAllgs = sum(ingsConcat.count(list(x)[0]) for x in allgPoss.values())
print(len(ingsConcat) - nAllgs)

allgList = sorted(allgPoss.keys())
print(','.join([list(allgPoss[x])[0] for x in allgList]))
