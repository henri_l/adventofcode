import time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

rows = open('2020/15.txt').read().split('\n')
nums = [int(x) for x in rows[0].split(',')]

mem = {nums[i]: i for i in range(len(nums)-1)}

n = 30000000
for i in range(len(nums)-1, n-1):
    prev = nums[i]
    nums.append(i-mem[prev] if prev in mem else 0)
    mem[prev] = i

print(nums[-1])
