import time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

data = [g.split('\n') for g in open('2020/16.txt').read().split('\n\n')]

rules = {}
for row in data[0]:
    (field, ranges) = re.findall('(.*): (.*)', row)[0]
    ranges = re.findall('(\d+)-(\d+)', ranges)
    rules[field] = [(int(r[0]), int(r[1])) for r in ranges]

my = [int(x) for x in data[1][1].split(',')]
nearby = [[int(x) for x in t.split(',')] for t in data[2][1:]]

def valid_value_for_field(val, field):
    for rng in rules[field]:
        if rng[0] <= val <= rng[1]:
            return True
    return False

def valid_ticket(ticket):
    for val in ticket:
        if not any(valid_value_for_field(val, f) for f in rules.keys()):
            return False
    return True

# part one
error_rate = 0
for t in nearby:
    for val in t:
        if not any(valid_value_for_field(val, f) for f in rules.keys()):
            error_rate += val
print("error rate:", error_rate)

# part two
all_tickets = [my] + nearby

def find_field_for_ix(ix, candidate_fields):
    valid_fields = []
    cf = candidate_fields.copy()
    for ticket in all_tickets:
        if valid_ticket(ticket):
            for field in list(cf):
                if not valid_value_for_field(ticket[ix], field):
                    cf.remove(field)
    if len(cf) == 1:
        return cf.pop()
    return None

all_fields = set(rules.keys())

field_order = [None]*len(all_fields)
while None in field_order:
    candidate_fields = [f for f in all_fields if f not in field_order]
    for i in range(len(field_order)):
        if field_order[i] is None:
            field_order[i] = find_field_for_ix(i, candidate_fields)

print(field_order)
print(reduce(lambda x,y: x*y, [val for val, field in zip(my, field_order) if field.startswith('departure')]))
