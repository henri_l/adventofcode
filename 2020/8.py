import time, re, copy
from functools import reduce
import aoctimer

rows = open('2020/8.txt').read().split('\n')
orig = [re.findall(r'(\w\w\w) ((?:\+|\-)\d+)', row) for row in rows]
orig = [[x[0][0], int(x[0][1]), False] for x in orig]
orig.append(['end', 0, False])

res = []
for j in range(len(orig)-1):
    if orig[j][0] == 'acc':
        continue
    cmd = copy.deepcopy(orig)
    cmd[j][0] = ('nop' if cmd[j][0] == 'jmp' else 'nop')
    acc = 0
    i = 0
    while True:
        [inst, val, flag] = cmd[i]
        cmd[i][2] = True
        if flag or inst == 'end':
            break
        elif inst == 'nop':
            i += 1
        elif inst == 'acc':
            acc += val
            i += 1
        elif inst == 'jmp':
            i += val
    if inst == 'end':
        res.append([acc, j])

print(res)
