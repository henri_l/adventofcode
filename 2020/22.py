import time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

rows = [row.split('\n') for row in open('2020/22.txt').read().split('\n\n')]

deck1 = [int(x) for x in rows[0][1:]]
deck2 = [int(x) for x in rows[1][1:]]

# part one
#while deck1 and deck2:
#    card1 = deck1.pop(0)
#    card2 = deck2.pop(0)
#    if card1 > card2:
#        deck1 += [card1, card2]
#    else:
#        deck2 += [card2, card1]
#print(sum((i+1)*n for i,n in enumerate(reversed(deck1 or deck2))))

# part two
def recurse_game(deck1, deck2):
    starting_positions = set()
    while deck1 and deck2:
        if (tuple(deck1), tuple(deck2)) in starting_positions:
            return True, []
        starting_positions.add((tuple(deck1.copy()), tuple(deck2.copy())))
        card1 = deck1.pop(0)
        card2 = deck2.pop(0)
        if len(deck1) >= card1 and len(deck2) >= card2:
            winner, _ = recurse_game(deck1[:card1], deck2[:card2])
        else:
            winner = card1 > card2
        if winner:
            deck1 += [card1, card2]
        else:
            deck2 += [card2, card1]
    return bool(deck1), deck1 or deck2
winner, deck = recurse_game(deck1, deck2)
print(sum((i+1)*n for i,n in enumerate(reversed(deck))))
