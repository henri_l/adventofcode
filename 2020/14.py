import time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

rows = open('2020/14.txt').read().split('\n')

part_one = False

memory = {}
nbits = 36
for row in rows:
    if row.startswith('mask'):
        mask = re.findall('mask = (.*)', row)[0]
    else:
        address, value = map(int, re.findall('mem\[(.*)\] = (.*)', row)[0])

        if part_one:
            value |= int(mask.replace('X', '0'), 2)
            value &= int(mask.replace('X', '1'), 2)
            memory[address] = value

        else: # part two
            address |= int(mask.replace('X', '0'), 2)
            floating_bits = [pow(2, i) for i,x in enumerate(reversed(mask)) if x=='X']
            for x in range(pow(2, len(floating_bits))):
                address |= sum([b for i,b in enumerate(floating_bits) if x & pow(2, i)])
                address &= ~sum([b for i,b in enumerate(floating_bits) if not x & pow(2, i)])
                memory[address] = value

print(sum(memory.values()))
