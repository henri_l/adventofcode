import time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

# part two might take a couple mins
part_two = True
cups = {} # singly linked list: ix->(cup, nextix)
cup2ix = {} # augments above with O(1) lookup
inp = [int(c) for c in '739862541']
for i, c in enumerate(inp):
    cups[i] = (int(c), i+1)
    cup2ix[int(c)] = i

if part_two:
    n2 = 1000000
    for i in range(9, n2):
        cups[i] = (i+1, (i+1)%n2)
        cup2ix[i+1] = i

lastix = max(cups.keys())
cups[lastix] = (cups[lastix][0], 0)
n = len(cups)
newix = n

def insertAfter(after, cup):
    global newix
    ix = cup2ix[after]
    nextix = cups[ix][1]
    cups[ix] = (after, newix)
    cups[newix] = (cup, nextix)
    cup2ix[cup] = newix
    newix += 1

def popAfter(cup):
    ix = cup2ix[cup]
    oldix = cups[ix][1]
    res, nextix = cups.pop(oldix)
    cups[ix] = (cup, nextix)
    return res

cur = cups[0][0]
for _ in range(10000000):
    picked = [popAfter(cur) for _ in range(3)]
    prevCup = lambda c: (c-2)%n+1
    destcup = prevCup(cur)
    while destcup in picked:
        destcup = prevCup(destcup)
    while picked:
        cup = picked.pop(0)
        insertAfter(destcup, cup)
        destcup = cup
    cur = cups[cups[cup2ix[cur]][1]][0]

if not part_two:
    ans = []
    anscup, ansix = cups[cups[cup2ix[1]][1]]
    while anscup != 1:
        ans.append(anscup)
        anscup, ansix = cups[ansix]
    print(ans)
else:
    ix1 = cups[cup2ix[1]][1]
    cup1, ix2 = cups[ix1]
    cup2 = cups[ix2][0]
    print(cup1*cup2)
