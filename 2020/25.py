import time, re, copy, math, operator
from functools import reduce, lru_cache
import aoctimer

rows = open('2020/25.txt').read().split('\n')
publickey1 = int(rows[0])
publickey2 = int(rows[1])

gen = 20201227

def key(loop, subject = 7):
    k = 1
    for _ in range(loop):
        k = (k*subject)%gen
    return k

def findloop(key):
    k = 1
    loop = 1
    while True:
        k = (k*7)%gen
        if k == key:
            return loop
        loop += 1

loop1 = findloop(publickey1)
loop2 = findloop(publickey2)

print(key(loop1, publickey2))
