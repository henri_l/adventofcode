use regex::Regex;
use std::fs;

const N: usize = 9;

fn stacks_regexp(n: usize) -> Regex {
    let re_stack = r"(\[\w\]|\s\s\s)";
    let mut re_stacks = String::new();
    for _ in 0..n {
        if !re_stacks.is_empty() {
            re_stacks.push_str(" ");
        }
        re_stacks.push_str(re_stack);
    }
    return Regex::new(&re_stacks).unwrap();
}

fn move_a(stacks: &mut [Vec<u8>; N], count: usize, from: usize, to: usize) {
    for _ in 0..count {
        let val = stacks[from].pop().unwrap();
        stacks[to].push(val);
    }
}

fn move_b(stacks: &mut [Vec<u8>; N], count: usize, from: usize, to: usize) {
    let pos = stacks[to].len();
    for _ in 0..count {
        let val = stacks[from].pop().unwrap();
        stacks[to].insert(pos, val);
    }
}

pub fn run() {
    let data = fs::read_to_string("input/5.txt").expect("read input");

    let mut stacks_orig: [Vec<u8>; N] = Default::default();
    let re = stacks_regexp(N);
    for x in re.captures_iter(data.as_str()) {
        for i in 0..N {
            let val = x[i + 1].as_bytes()[1];
            if val.is_ascii_uppercase() {
                stacks_orig[i].insert(0, val)
            }
        }
    }

    let re2 = Regex::new(r"(?m)^move (\d*) from (\d*) to (\d*)$").unwrap();
    for func in [move_a, move_b] {
        let mut stacks = stacks_orig.clone();
        for x in re2.captures_iter(data.as_str()) {
            let count = x[1].parse::<usize>().unwrap();
            let from = x[2].parse::<usize>().unwrap() - 1;
            let to = x[3].parse::<usize>().unwrap() - 1;
            func(&mut stacks, count, from, to);
        }

        let res_chars = Vec::from_iter(stacks.iter().map(|x| x.last().unwrap()).cloned());
        println!("{}", String::from_utf8_lossy(res_chars.as_slice()).to_string());
    }
}
