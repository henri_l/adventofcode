use crate::utils::Point;
use regex::Regex;
use std::fs;

struct Sensor {
    loc: Point,
    bacon: Point,
}

fn manhattan_dist(a: Point, b: Point) -> i64 {
    return (a.x - b.x).abs() + (a.y - b.y).abs();
}

fn is_outside(sensors: &Vec<Sensor>, loc: Point) -> bool {
    return sensors
        .iter()
        .map(|s| manhattan_dist(s.loc, loc) > manhattan_dist(s.loc, s.bacon))
        .all(|x| x);
}

pub fn run() {
    let data = fs::read_to_string("input/15.txt").expect("read input");

    let re_str = r"(?m)^Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)$";
    let re = Regex::new(re_str).unwrap();

    let sensors = data
        .lines()
        .map(|line| re.captures(line).unwrap())
        .map(|c| [1, 2, 3, 4].map(|i| c[i].parse::<i64>().unwrap()))
        .map(|xs| Sensor { loc: Point { x: xs[0], y: xs[1] }, bacon: Point { x: xs[2], y: xs[3] } })
        .collect::<Vec<_>>();

    let x0 = sensors.iter().map(|s| s.loc.x - manhattan_dist(s.loc, s.bacon)).min().unwrap();
    let x1 = sensors.iter().map(|s| s.loc.x + manhattan_dist(s.loc, s.bacon)).max().unwrap();
    let y = 2000000;
    println!(
        "{}",
        (x0..x1 + 1)
            .map(|x| Point { x: x, y: y })
            .filter(|&p| !is_outside(&sensors, p))
            .filter(|&p| sensors.iter().filter(|s| s.bacon == p).count() == 0)
            .count()
    );

    let max = 4000000;
    for s in sensors.iter() {
        let d_outer = manhattan_dist(s.loc, s.bacon) + 1;
        for (mut p, dp) in [
            (s.loc + Point { x: d_outer, y: 0 }, Point { x: -1, y: 1 }),
            (s.loc + Point { x: 0, y: d_outer }, Point { x: -1, y: -1 }),
            (s.loc + Point { x: -d_outer, y: 0 }, Point { x: 1, y: -1 }),
            (s.loc + Point { x: 0, y: -d_outer }, Point { x: 1, y: 1 }),
        ] {
            for _ in 0..d_outer {
                p = p + dp;
                if 0 <= p.x && p.x <= max && 0 <= p.y && p.y <= max && is_outside(&sensors, p) {
                    println!("{}", max * p.x + p.y);
                    return;
                }
            }
        }
    }
}
