use regex::Regex;
use std::fs;

pub fn run() {
    let data = fs::read_to_string("input/10.txt").expect("read input");
    let mut lines = data.lines();

    let re_addx = Regex::new(r"(?m)^addx (-?\d*)$").unwrap();

    let mut cycle = 0;
    let mut x = 1;
    let mut addx_amount = 0;
    let mut addx_wait = false;
    let mut signal_strength = 0;
    let mut crt = String::new();
    loop {
        cycle += 1;

        if addx_wait {
            addx_wait = false;
        }
        else if let Some(line) = lines.next() {
            if let Some(m) = re_addx.captures(line) {
                addx_wait = true;
                addx_amount = m[1].parse::<i32>().unwrap();
            }
            // else noop
        }
        else {
            break;
        }

        if (cycle + 20) % 40 == 0 {
            signal_strength += x * cycle;
        }
        let crt_pixel = [-1, 0, 1].contains(&((cycle - 1) % 40 - x));
        crt.push(if crt_pixel { '|' } else { ' ' });

        if !addx_wait {
            x += addx_amount;
            addx_amount = 0;
        }
    }
    println!("{}", signal_strength);
    for i in 0..6 {
        println!("{}", &crt[40 * i..40 * (i + 1)]);
    }
}
