use regex::Regex;
use std::fs;

fn to_arr(c: regex::Captures) -> [i32; 4] {
    // TODO: how to map to constant size array?
    return [
        c[1].parse::<i32>().unwrap(),
        c[2].parse::<i32>().unwrap(),
        c[3].parse::<i32>().unwrap(),
        c[4].parse::<i32>().unwrap(),
    ];
}

pub fn run() {
    let data = fs::read_to_string("input/4.txt").expect("read input");
    // (?m) = multi-line mode (support ^ and $)
    let re = Regex::new(r"(?m)^(\d*)-(\d*),(\d*)-(\d*)$").unwrap();
    let indices = re.captures_iter(data.as_str()).map(to_arr).collect::<Vec<_>>();

    println!(
        "{}",
        indices
            .iter()
            .filter(|[l1, r1, l2, r2]| (l1 <= l2 && r2 <= r1) || (l2 <= l1 && r1 <= r2))
            .count()
    );

    println!("{}", indices.iter().filter(|[l1, r1, l2, r2]| !(r1 < l2 || r2 < l1)).count());
}
