use {std::cmp, std::fs};

fn try_get_mat(mat: &Vec<Vec<i32>>, i: i32, j: i32) -> Option<i32> {
    if i < 0 || j < 0 || mat.len() <= i as usize || mat[i as usize].len() <= j as usize {
        return None;
    }
    return Some(mat[i as usize][j as usize]);
}

fn score_tree(trees: &Vec<Vec<i32>>, i_tree: usize, mut j_tree: usize, part_a: bool) -> usize {
    let h = trees[i_tree][j_tree];
    let mut score = 1; // for part a, 0 means "visible"
    for (di, dj) in [(0, -1), (0, 1), (-1, 0), (1, 0)] {
        let mut i = i_tree as i32 + di;
        let mut j = j_tree as i32 + dj;
        let mut direction_score = 0;
        while let Some(tree) = try_get_mat(trees, i, j) {
            if !part_a {
                direction_score += 1;
            }
            if tree >= h {
                if part_a {
                    direction_score = 1; // blocked
                }
                break;
            }
            i += di;
            j += dj;
        }
        score *= direction_score;
    }
    return if part_a { 1 - cmp::min(1, score) } else { score };
}

fn char2i32(c: char) -> i32 {
    return c.to_string().parse::<i32>().unwrap();
}

pub fn run() {
    let data = fs::read_to_string("input/8.txt").expect("read input");
    let mut trees =
        data.lines().map(|s| s.chars().map(char2i32).collect::<Vec<_>>()).collect::<Vec<_>>();

    for part_a in [true, false] {
        let mut visible = 0;
        for i in 0..trees.len() {
            for j in 0..trees.first().unwrap().len() {
                let score = score_tree(&trees, i, j, part_a);
                visible = if part_a { visible + score } else { cmp::max(visible, score) };
            }
        }
        println!("{}", visible)
    }
}
