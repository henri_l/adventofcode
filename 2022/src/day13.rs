use std::{cmp::Ordering, fs};

fn pop_next(arr: &[u8]) -> (&[u8], &[u8]) {
    let mut level = 0;
    for end in 0..arr.len() {
        if arr[end] == b',' && level == 0 {
            return (&arr[0..end], &arr[end + 1..arr.len()]);
        }
        else if arr[end] == b'[' {
            level += 1;
        }
        else if arr[end] == b']' {
            level -= 1;
        }
    }
    return (arr, &[]);
}

fn unwrap_elem(elem: &[u8]) -> &[u8] {
    if elem.first().unwrap() == &b'[' {
        return &elem[1..elem.len() - 1];
    }
    return elem;
}

fn order(left: &&[u8], right: &&[u8]) -> Ordering {
    if left.is_empty() && right.is_empty() {
        return Ordering::Equal;
    }
    else if left.is_empty() {
        return Ordering::Less;
    }
    else if right.is_empty() {
        return Ordering::Greater;
    }
    let (elem_left, rest_left) = pop_next(*left);
    let (elem_right, rest_right) = pop_next(*right);
    if let Some(left_num) = String::from_utf8_lossy(elem_left).parse::<i32>().ok() {
        if let Some(right_num) = String::from_utf8_lossy(elem_right).parse::<i32>().ok() {
            return left_num.cmp(&right_num).then(order(&rest_left, &rest_right));
        }
    }
    return order(&unwrap_elem(elem_left), &unwrap_elem(elem_right))
        .then(order(&rest_left, &rest_right));
}

pub fn run() {
    let data = fs::read_to_string("input/13.txt").expect("read input");
    let mut lines = data.lines().peekable();

    let mut res = 0;
    let mut i = 1;
    let mut all_lines = Vec::new();
    while lines.peek().is_some() {
        let left = lines.next().unwrap().as_bytes();
        let right = lines.next().unwrap().as_bytes();
        lines.next(); // skip

        all_lines.extend([left, right]);

        if order(&left, &right) != Ordering::Greater {
            res += i;
        }
        i += 1;
    }
    println!("{}", res);

    let dividers = ["[[2]]".as_bytes(), "[[6]]".as_bytes()];
    all_lines.extend(dividers);
    all_lines.sort_by(order);
    let indices = dividers.iter().map(|d| all_lines.iter().position(|x| x == d).unwrap() + 1);
    println!("{}", indices.reduce(|x, y| x * y).unwrap());
}
