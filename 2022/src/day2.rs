use std::fs;

// 0 = rock; 1 = paper; 2 = scissors
fn val(item: char) -> usize {
    return ['A', 'B', 'C', 'X', 'Y', 'Z'].iter().position(|&x| x == item).unwrap() % 3;
}

fn score1(me: usize, op: usize) -> usize {
    return me
        + 1
        + match (3 + me - op) % 3 {
            0 => 3,
            1 => 6,
            2 => 0,
            _ => panic!(),
        };
}

fn score2(me: usize, op: usize) -> usize {
    let choice = match me {
        0 => (op + 2) % 3,
        1 => op,
        2 => (op + 1) % 3,
        _ => panic!(),
    };
    return score1(choice, op);
}

pub fn run() {
    let data = fs::read_to_string("input/2.txt").expect("read input");
    let plan = data
        .lines()
        .map(|l| (val(l.chars().nth(0).unwrap()), val(l.chars().nth(2).unwrap())))
        .collect::<Vec<_>>();

    println!("{}", plan.iter().map(|x| score1(x.1, x.0)).sum::<usize>());
    println!("{}", plan.iter().map(|x| score2(x.1, x.0)).sum::<usize>());
}
