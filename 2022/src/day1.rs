use std::fs;

pub fn run() {
    let data = fs::read_to_string("input/1.txt").expect("read input");

    let mut elves = Vec::new();
    elves.push(0);
    for line in data.lines() {
        if line.is_empty() {
            elves.push(0);
        }
        else {
            let calories = line.parse::<i64>().unwrap();
            *elves.last_mut().unwrap() += calories;
        }
    }

    elves.sort();

    println!("{}", elves.last().unwrap());
    println!("{}", elves.iter().rev().take(3).sum::<i64>());
}
