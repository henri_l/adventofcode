use crate::utils::Point;
use regex::Regex;
use std::{collections::HashSet, fs};

fn get_walls() -> HashSet<(Point, Point)> {
    let data = fs::read_to_string("input/14.txt").expect("read input");
    let re_coord = Regex::new(r"(\d*),(\d*)").unwrap();
    let mut res = HashSet::new();
    for line in data.lines() {
        let mut prev = Option::None;
        for m in re_coord.captures_iter(line) {
            let coord = Point { x: m[1].parse::<i64>().unwrap(), y: m[2].parse::<i64>().unwrap() };
            if let Some(from) = prev {
                res.insert((from, coord));
            }
            prev = Some(coord);
        }
    }
    return res;
}

fn get_rocks_and_floor() -> (HashSet<Point>, i64) {
    let walls = get_walls();
    let mut rocks = HashSet::new();
    for (start, end) in walls {
        for x in std::cmp::min(start.x, end.x)..std::cmp::max(start.x, end.x) + 1 {
            for y in std::cmp::min(start.y, end.y)..std::cmp::max(start.y, end.y) + 1 {
                rocks.insert(Point { x: x, y: y });
            }
        }
    }

    let floor = rocks.iter().map(|p| p.y).max().unwrap();
    return (rocks, floor + 2);
}

const START: Point = Point { x: 500, y: 0 };

fn drop_sand(blocked: &HashSet<Point>, floor: i64, floor_is_void: bool) -> Point {
    let mut sand = START;
    let mut dropped = true;
    while !blocked.contains(&sand) && sand.y != floor && dropped {
        dropped = false;
        for dp in [Point { x: 0, y: 1 }, Point { x: -1, y: 1 }, Point { x: 1, y: 1 }] {
            let next = sand + dp;
            if !blocked.contains(&next) && (floor_is_void || next.y != floor) {
                sand = next;
                dropped = true;
                break;
            }
        }
    }
    return sand;
}

fn pour_sand(rocks: &HashSet<Point>, floor: i64, floor_is_void: bool) -> HashSet<Point> {
    let mut blocked = rocks.clone(); // rocks + sand
    let mut res = HashSet::new();
    loop {
        let sand = drop_sand(&blocked, floor, floor_is_void);
        if sand.y != floor {
            blocked.insert(sand);
            res.insert(sand);
        }
        if sand == START || sand.y == floor {
            break;
        }
    }
    return res;
}

pub fn run() {
    let (rocks, floor) = get_rocks_and_floor();
    println!("{}", pour_sand(&rocks, floor, true).len());
    println!("{}", pour_sand(&rocks, floor, false).len());
}
