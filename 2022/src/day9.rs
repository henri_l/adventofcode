use crate::utils::Point;
use regex::Regex;
use std::{cmp, collections::HashSet, fs};

fn parse_dir(dir: &str) -> Point {
    match dir {
        "L" => Point { x: -1, y: 0 },
        "R" => Point { x: 1, y: 0 },
        "U" => Point { x: 0, y: -1 },
        "D" => Point { x: 0, y: 1 },
        _ => panic!("unknown dir {}", dir),
    }
}

fn catch_up(tail: Point, head: Point) -> Point {
    let diff = head - tail;
    let bound = |dim| cmp::max(-1, cmp::min(1, dim));
    let step = Point { x: bound(diff.x), y: bound(diff.y) };
    if step != diff {
        return tail + step;
    }
    return tail;
}

pub fn count_visits(rope_len: usize) -> usize {
    let data = fs::read_to_string("input/9.txt").expect("read input");
    let re = Regex::new(r"(?m)^(L|R|U|D) (\d*)$").unwrap();
    let mut rope = vec![Point { x: 0, y: 0 }; rope_len];
    let mut visited = HashSet::new();
    visited.insert(*rope.last().unwrap());
    for row in data.lines() {
        let captures = re.captures(row).unwrap();
        let dir = parse_dir(&captures[1]);
        let steps = captures[2].parse::<usize>().unwrap();
        for _ in 0..steps {
            rope[0] = rope[0] + dir;
            for i in 0..rope.len() - 1 {
                let head = rope[i];
                let tail = rope[i + 1];
                rope[i + 1] = catch_up(tail, head);
                if i == rope.len() - 2 {
                    visited.insert(rope[i + 1]);
                }
            }
        }
    }
    return visited.len();
}

pub fn run() {
    println!("{}", count_visits(2));
    println!("{}", count_visits(10));
}
