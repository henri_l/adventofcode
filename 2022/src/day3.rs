use std::collections::HashSet;
use std::fs;

fn priority(item: u8) -> u8 {
    return match item {
        b'a'..=b'z' => item - b'a' + 1,
        b'A'..=b'Z' => item - b'A' + 27,
        _ => panic!(),
    };
}

fn slice2set(items: &[u8]) -> HashSet<u8> {
    return HashSet::from_iter(items.iter().cloned());
}

fn pockets(bag: &[u8]) -> (HashSet<u8>, HashSet<u8>) {
    let n = bag.len() / 2;
    return (slice2set(&bag[0..n]), slice2set(&bag[n..]));
}

pub fn run() {
    let data = fs::read_to_string("input/3.txt").expect("read input"); // unwrap?
    let bags = data.lines().map(|x| x.as_bytes()).collect::<Vec<_>>();

    println!(
        "{}",
        bags.iter()
            .cloned()
            .map(pockets)
            .map(|(left, right)| *left.intersection(&right).next().unwrap())
            .map(priority)
            .map(usize::from)
            .sum::<usize>()
    );

    let mut sum_b: usize = 0;
    for group in bags.chunks_exact(3) {
        let a = slice2set(group[0]);
        let b = slice2set(group[1]);
        let c = slice2set(group[2]);
        let common = *a.iter().filter(|x| b.contains(x) && c.contains(x)).next().unwrap();
        sum_b += priority(common) as usize;
    }
    println!("{}", sum_b);
}
