use lazy_static::lazy_static;
use regex::Regex;
use std::{collections::HashMap, fs};

#[derive(Default)]
struct Folder {
    files: HashMap<String, usize>,
    folders: HashMap<String, Folder>,
}

impl Folder {
    // return true if needs to fall back (out of commands or "cd /")
    fn parse_commands<'a, I>(&mut self, commands: &mut I) -> bool
    where
        I: Iterator<Item = &'a str>,
    {
        lazy_static! {
            static ref RE_CDBASE: Regex = Regex::new(r"^\$ cd /$").unwrap();
            static ref RE_CDBACK: Regex = Regex::new(r"^\$ cd ..$").unwrap();
            static ref RE_CD: Regex = Regex::new(r"^\$ cd (\w*)$").unwrap();
            static ref RE_LS: Regex = Regex::new(r"^\$ ls$").unwrap();
            static ref RE_DIR: Regex = Regex::new(r"^dir (.*)$").unwrap();
            static ref RE_FILE: Regex = Regex::new(r"^(\d*) (.*)$").unwrap();
        }

        while let Some(line) = commands.next() {
            if RE_CDBASE.is_match(line) {
                return true;
            }
            else if RE_CDBACK.is_match(line) {
                return false;
            }
            else if let Some(m) = RE_CD.captures(line) {
                if self.folders.entry(m[1].to_string()).or_default().parse_commands(commands) {
                    return true;
                }
            }
            else if RE_LS.is_match(line) || RE_DIR.is_match(line) {
                // ignore
            }
            else if let Some(m) = RE_FILE.captures(line) {
                self.files.insert(m[2].to_string(), m[1].parse::<usize>().unwrap());
            }
            else {
                panic!("Unexpected line: {}", line);
            }
        }
        return true;
    }

    fn foldersizes(&self) -> Vec<usize> {
        let mut res = Vec::new();
        let mut mysize = 0;
        for filesize in self.files.values() {
            mysize += filesize;
        }
        for folder in self.folders.values() {
            let mut subsizes = folder.foldersizes();
            mysize += subsizes.last().unwrap(); // own size is always at back
            res.append(&mut subsizes);
        }
        res.push(mysize);
        return res;
    }
}

pub fn run() {
    let data = fs::read_to_string("input/7.txt").expect("read input");
    let mut lines = data.lines().peekable();

    let mut base: Folder = Default::default();
    while lines.peek().is_some() {
        base.parse_commands(&mut lines);
    }

    let foldersizes = base.foldersizes();
    println!("{}", foldersizes.iter().filter(|x| *x <= &100000).sum::<usize>());

    let free_required = 30000000 - (70000000 - foldersizes.last().unwrap());
    println!("{}", foldersizes.iter().filter(|x| *x >= &free_required).min().unwrap());
}
