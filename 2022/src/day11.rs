use std::collections::VecDeque;

struct Monkey {
    items: VecDeque<i64>,
    oper: Box<dyn Fn(i64) -> i64>,
    test_divisor: i64,
    targets: [usize; 2],
}

fn monkey_business(rounds: usize, worry_divisor: i64) -> usize {
    let mut monkeys = Vec::from_iter([
        Monkey {
            items: VecDeque::from_iter([75, 63]),
            oper: Box::new(|old| old * 3),
            test_divisor: 11,
            targets: [2, 7],
        },
        Monkey {
            items: VecDeque::from_iter([65, 79, 98, 77, 56, 54, 83, 94]),
            oper: Box::new(|old| old + 3),
            test_divisor: 2,
            targets: [0, 2],
        },
        Monkey {
            items: VecDeque::from_iter([66]),
            oper: Box::new(|old| old + 5),
            test_divisor: 5,
            targets: [5, 7],
        },
        Monkey {
            items: VecDeque::from_iter([51, 89, 90]),
            oper: Box::new(|old| old * 19),
            test_divisor: 7,
            targets: [4, 6],
        },
        Monkey {
            items: VecDeque::from_iter([75, 94, 66, 90, 77, 82, 61]),
            oper: Box::new(|old| old + 1),
            test_divisor: 17,
            targets: [1, 6],
        },
        Monkey {
            items: VecDeque::from_iter([53, 76, 59, 92, 95]),
            oper: Box::new(|old| old + 2),
            test_divisor: 19,
            targets: [3, 4],
        },
        Monkey {
            items: VecDeque::from_iter([81, 61, 75, 89, 70, 92]),
            oper: Box::new(|old| old * old),
            test_divisor: 3,
            targets: [1, 0],
        },
        Monkey {
            items: VecDeque::from_iter([81, 86, 62, 87]),
            oper: Box::new(|old| old + 8),
            test_divisor: 13,
            targets: [5, 3],
        },
    ]);

    let mut overflow_divisor = 1;
    for monkey in monkeys.iter() {
        overflow_divisor *= monkey.test_divisor;
    }

    let mut inspections = vec![0; monkeys.len()];
    for _ in 0..rounds {
        for i in 0..monkeys.len() {
            inspections[i] += monkeys[i].items.len();
            while let Some(mut item) = monkeys[i].items.pop_front() {
                item = (monkeys[i].oper)(item) / worry_divisor;
                item %= overflow_divisor * worry_divisor; // avoid overflow
                let tgt = monkeys[i].targets[(item % monkeys[i].test_divisor == 0) as usize];
                monkeys[tgt].items.push_back(item);
            }
        }
    }

    inspections.sort();
    inspections.reverse();
    return inspections[0] * inspections[1];
}

pub fn run() {
    println!("{}", monkey_business(20, 3));
    println!("{}", monkey_business(10000, 1));
}
