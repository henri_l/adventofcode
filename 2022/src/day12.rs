use crate::utils::Point;
use std::{
    collections::{HashMap, HashSet},
    fs,
};

fn get_heights() -> (HashMap<Point, u8>, Point, Point, HashSet<Point>) {
    let data = fs::read_to_string("input/12.txt").expect("read input");
    let mut heights = HashMap::new();
    let mut start = Default::default();
    let mut end = Default::default();
    let mut a_positions = HashSet::new();
    let mut y = 0;
    for line in data.lines() {
        let mut x = 0;
        for c in line.as_bytes() {
            if *c == b'S' {
                start = Point { x: x, y: y };
                a_positions.insert(Point { x: x, y: y });
                heights.insert(Point { x: x, y: y }, b'a');
            }
            else if *c == b'E' {
                end = Point { x: x, y: y };
                heights.insert(Point { x: x, y: y }, b'z');
            }
            else {
                if *c == b'a' {
                    a_positions.insert(Point { x: x, y: y });
                }
                heights.insert(Point { x: x, y: y }, *c);
            }
            x += 1;
        }
        y += 1;
    }
    return (heights, start, end, a_positions);
}

fn find_distance(heights: &HashMap<Point, u8>, start: Point, end: Point) -> usize {
    let mut distances = HashMap::<Point, usize>::from_iter([(start, 0)]);
    let mut boundary = HashSet::from_iter([(start)]);
    loop {
        let mut new_boundary = HashSet::new();
        for p in boundary {
            let p_height = heights[&p];
            let p_distance = distances[&p];
            for dp in [
                Point { x: 1, y: 0 },
                Point { x: -1, y: 0 },
                Point { x: 0, y: 1 },
                Point { x: 0, y: -1 },
            ] {
                let next = p + dp;
                if distances.contains_key(&next) {
                    continue;
                }
                if let Some(height) = heights.get(&next) {
                    if *height <= p_height + 1 {
                        let next_distance = p_distance + 1;
                        if next == end {
                            return next_distance;
                        }
                        distances.insert(next, next_distance);
                        new_boundary.insert(next);
                    }
                }
            }
        }
        if new_boundary.is_empty() {
            return usize::MAX;
        }
        boundary = new_boundary;
    }
}

pub fn run() {
    let (heights, start, end, a_positions) = get_heights();
    let steps = find_distance(&heights, start, end);
    println!("{}", steps);
    println!("{}", a_positions.iter().map(|p| find_distance(&heights, *p, end)).min().unwrap());
}
