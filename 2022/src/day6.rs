use std::fs;

fn find_marker(data: &[u8], len: usize) -> usize {
    for i in len..data.len() {
        let marker = &data[i - len..i];
        if marker.iter().map(|x| marker.iter().filter(|y| *y == x).count()).all(|c| c == 1) {
            return i;
        }
    }
    panic!();
}

pub fn run() {
    let data = fs::read_to_string("input/6.txt").expect("read input");
    let chars = data.as_bytes();
    println!("{}", find_marker(chars, 4));
    println!("{}", find_marker(chars, 14));
}
