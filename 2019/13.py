import sys, os, time, re, copy, math
from fractions import Fraction as F
from functools import reduce, lru_cache, cmp_to_key
import aoctimer
from aocutils import Point
from intcode import Intcode

data = open('2019/13.txt').read()
code = Intcode(data)

def printGrid(grid):
    xmin = ymin = xmax = ymax = 0
    for y in range(0, 24):
        row = ''
        for x in range(0, 42):
            row += str(grid.setdefault(Point(x, y), 0))
        print(row)
    print()

grid_a = {}
code_a = code.copy()
while True:
    output = code_a.run(3)
    if not output:
        break
    x, y, val = output
    grid_a[Point(x, y)] = val

print(list(grid_a.values()).count(2))

def horizPos(grid, x):
    for pos, val in grid.items():
        if val == x:
            return pos[0]

grid_b = {}
code_b = code.copy()
code_b.code[0] = 2
code_b.input(0) # any initial input works
while True:
    output = code_b.run(3)
    if not output:
        break
    x, y, val = output
    if x==-1 and y==0:
        score = val
        print('Score: ', score)
    else:
        grid_b[Point(x, y)] = val
        if val in (3, 4):
            x_paddle = horizPos(grid_b, 3)
            x_ball = horizPos(grid_b, 4)
            if x_paddle is not None and x_ball is not None:
                #if val == 3:
                #    printGrid(grid_b)
                if val == 4 and not code_b.inputs:
                    sign = lambda x: -1 if x<0 else 1 if x>0 else 0
                    code_b.input(sign(x_ball - x_paddle))
print('Final score: ', score)
