import sys, os, time, re, copy, math
from functools import reduce, lru_cache
import aoctimer
from intcode import Intcode

data = open(os.path.join(sys.path[0], '2.txt')).read()
code = Intcode(data)

a_code = code.copy()
a_code.code[1] = 12
a_code.code[2] = 2
a_code.run()
print(a_code.code[0])

def b():
    for noun in range(100):
        for verb in range(100):
            b_code = code.copy()
            b_code.code[1] = noun
            b_code.code[2] = verb
            b_code.run()
            if b_code.code[0] == 19690720:
                return 100*noun + verb
    raise RuntimeError("not finished")

print(b())
