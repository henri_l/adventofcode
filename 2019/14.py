import sys, os, time, re, copy, math
from fractions import Fraction as F
from functools import reduce, lru_cache, cmp_to_key
import aoctimer

rows = open('2019/14.txt').read().split('\n')
conv = {}
for row in rows:
    inputs, output = row.split('=>')
    p = '(\d+) (\w+)'
    [(n_out, m_out)] = re.findall(p, output)
    inputs = re.findall(p, inputs)
    inputs = [(int(n), m) for n, m in inputs]
    conv[m_out] = (int(n_out), inputs)

def make(mineral, n, extras = None):
    if extras is None:
        extras = {}
    if mineral == 'ORE':
        return n
    else:
        (n_out, inputs) = conv[mineral]
        available = extras.setdefault(mineral, 0)
        use_available = min(available, n)
        extras[mineral] -= use_available
        n -= use_available
        batches = math.ceil(n/n_out)
        extras[mineral] += batches*n_out - n
        ore_used = 0
        for n_in, m_in in inputs:
            ore_used += make(m_in, batches*n_in, extras)
        return ore_used
print(make('FUEL', 1))

b_ore = 1000000000000
b_ans = 2
step = 1000000
while True:
    lower = make('FUEL', b_ans)
    upper = make('FUEL', b_ans+step)
    if lower <= b_ore and upper > b_ore:
        if step == 1:
            break
        step = step//2
    else:
        b_ans += -step if lower > b_ore else step
print(b_ans)
