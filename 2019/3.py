import sys, os, time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

data = open(os.path.join(sys.path[0], '3.txt')).read().split('\n')
moves = [[(x[0], int(x[1:])) for x in path.split(',')] for path in data]

def path(moves):
    pos = (0, 0)
    path = [pos]
    for move in moves:
        (d, len) = move
        if d == 'U':
            pos = (pos[0]+len, pos[1])
        elif d == 'D':
            pos = (pos[0]-len, pos[1])
        elif d == 'L':
            pos = (pos[0], pos[1]-len)
        elif d == 'R':
            pos = (pos[0], pos[1]+len)
        path.append(pos)
    return path

paths = [path(x) for x in moves]

min_dist = 100000000
min_dist2 = 100000000
a_path_dist = 0
for i in range(len(paths[0])-1):
    a0 = paths[0][i]
    a1 = paths[0][i+1]
    ax = sorted([a0[0], a1[0]])
    ay = sorted([a0[1], a1[1]])
    b_path_dist = 0
    for j in range(len(paths[1])-1):
        if i>0 or j>0:
            b0 = paths[1][j]
            b1 = paths[1][j+1]
            bx = sorted([b0[0], b1[0]])
            by = sorted([b0[1], b1[1]])
            x_isct = (max(ax[0], bx[0]), min(ax[1], bx[1]))
            y_isct = (max(ay[0], by[0]), min(ay[1], by[1]))
            if x_isct[0] <= x_isct[1] and y_isct[0] <= y_isct[1]:
                closest = lambda a, b: 0 if (a<0 == b<0) else a if abs(a) < abs(b) else b
                x_closest = closest(*x_isct)
                y_closest = closest(*y_isct)
                dist = abs(x_closest) + abs(y_closest)
                # part two
                dist2a = a_path_dist + abs(a0[0]-x_closest) + abs(a0[1]-y_closest)
                dist2b = b_path_dist + abs(b0[0]-x_closest) + abs(b0[1]-y_closest)
                min_dist = min(min_dist, dist)
                min_dist2 = min(min_dist2, dist2a+dist2b)
            b_path_dist += abs(b0[0] - b1[0]) + abs(b0[1] - b1[1])
    a_path_dist += abs(a0[0] - a1[0]) + abs(a0[1] - a1[1])

print(min_dist)
print(min_dist2)
