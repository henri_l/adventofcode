import sys, os, time, re, copy, math
from fractions import Fraction as F
from functools import reduce, lru_cache, cmp_to_key
import aoctimer
from intcode import Intcode

data = open(os.path.join(sys.path[0], '11.txt')).read()
code = Intcode(data)

pos = (0, 0)
dir = 0 # 0=up, 1=left, 2=down, 3=right
floor = dict()
coords_painted = set()

part_two = True
if part_two:
    floor[(0,0)] = 1

while True:
    # check camera
    code.input(floor.setdefault(pos, 0))
    # run code
    output = code.run(2)
    if not output:
        break
    else:
        [paint_color, turn] = output
        # paint
        floor[pos] = paint_color
        coords_painted.add(pos)
        # turn
        dir = (dir-1 if turn else dir+1)%4
        # move
        pos = (
            pos[0] + (-1 if dir==1 else 1 if dir==3 else 0),
            pos[1] + (-1 if dir==0 else 1 if dir==2 else 0),
        )

if not part_two:
    print(len(coords_painted))
else:
    x_min = min(c[0] for c in floor.keys())
    x_max = max(c[0] for c in floor.keys())
    y_min = min(c[1] for c in floor.keys())
    y_max = max(c[1] for c in floor.keys())
    for y in range(y_min, y_max+1):
        row = ''
        for x in range(x_min, x_max+1):
            row += '#' if floor.get((x,y), 0) == 1 else ' '
        print(row)
