import sys, os, time, re, copy, math
from functools import reduce, lru_cache
import aoctimer
from intcode import Intcode

data = open(os.path.join(sys.path[0], '7.txt')).read()
code = Intcode(data)

def generate_phase_settings(res, template, nums):
    n = template.count(None)
    if n == 0:
        res.append(template)
    else:
        next_nums = nums.copy()
        num = next_nums.pop(0)
        for i in range(len(template)):
            if template[i] is None:
                next_template = template.copy()
                next_template[i] = num
                generate_phase_settings(res, next_template, next_nums)
phase_settings_1 = []
generate_phase_settings(phase_settings_1, [None]*5, [0,1,2,3,4])

amplifiers = [None, None, None, None, None]

def init_amplifiers():
    for i in range(len(amplifiers)):
        amplifiers[i] = code.copy()

def signal(phase_settings, input):
    for amp in amplifiers:
        if phase_settings:
            amp.input(phase_settings.pop(0))
        amp.input(input)
        output = amp.run(True)
        if not output:
            return
        input = output[-1]
    return input

# part one
max_signal = 0
for s in phase_settings_1:
    init_amplifiers()
    max_signal = max(max_signal, signal(s, 0))
print(max_signal)

# part two
phase_settings_2 = []
generate_phase_settings(phase_settings_2, [None]*5, [5,6,7,8,9])
max_signal_2 = 0

for phase_settings in phase_settings_2:
    max_signal_e = 0
    res = 0
    init_amplifiers()
    while res is not None:
        res = signal(phase_settings, res)
        if res is not None:
            max_signal_e = max(max_signal_e, res)
        else:
            break
    max_signal_2 = max(max_signal_2, max_signal_e)
print(max_signal_2)
