import sys, os, time, re, copy, math
from fractions import Fraction as F
from functools import reduce, lru_cache, cmp_to_key
import aoctimer
from aocutils import Point, lcm

rows = open('2019/12.txt').read().split('\n')

moons = []
for row in rows:
    [(x, y, z)] = re.findall('<x=(.*), y=(.*), z=(.*)>', row)
    moons.append([Point(int(x), int(y), int(z)), Point(0, 0, 0)])

def gravity(moon, other):
    return Point(*map(lambda x: -1 if x<0 else 1 if x>0 else 0, other[0] - moon[0]))

def energy(moon):
    pot = sum([abs(x) for x in moon[0]])
    kin = sum([abs(x) for x in moon[1]])
    return pot*kin

def step(moons):
    for moon in moons:
        for other in moons:
            if other is not moon:
                moon[1] += gravity(moon, other)
    for moon in moons:
        moon[0] += moon[1]

# part one

for _ in range(1000):
    step(moons)
print(sum([energy(moon) for moon in moons]))

# part two

def findCycle(moons):
    tmp = [[pos, vel] for pos, vel in moons]
    i = 1
    while True:
        step(tmp)
        if tmp == moons:
            return i
        i += 1

def onedim(moons, dim):
    return [[Point(moon[0][dim]), Point(moon[1][dim])] for moon in moons]

cycle_x = findCycle(onedim(moons, 0))
cycle_y = findCycle(onedim(moons, 1))
cycle_z = findCycle(onedim(moons, 2))
print(reduce(lcm, (cycle_x, cycle_y, cycle_z)))
