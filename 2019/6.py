import sys, os, time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

data = open(os.path.join(sys.path[0], '6.txt')).read().split('\n')

orbits = {}
for row in data:
    (big, small) = re.findall('(.*)\)(.*)', row)[0]
    orbits[small] = big

dists = {}
def count_orbits(x, dist=None, check_mode=False):
    if x not in orbits:
        return 0
    else:
        big = orbits[x]
        if check_mode and big in dists:
            print(dist + dists[big])
            return 999
        else:
            if dist is not None:
                dists[big] = dist
            new_dist = dist+1 if dist is not None else None
            return 1 + count_orbits(big, new_dist, check_mode)

print(sum([count_orbits(k) for k in list(orbits.keys())]))
count_orbits('YOU', 0, False)
count_orbits('SAN', 0, True)
