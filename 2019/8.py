import sys, os, time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

data = open(os.path.join(sys.path[0], '8.txt')).read()

nRows = 6
nCols = 25

def count(layer, d):
    return sum(map(lambda s: s.count(d), layer))

layers = []
i = 0
while i < len(data):
    layer = []
    for _ in range(nRows):
        layer.append(data[i:i+nCols])
        i += nCols
    layers.append(layer.copy())

# part one
min1 = 100000
ix1 = None
for layerIx in range(len(layers)):
    zeros = count(layers[layerIx], '0')
    if zeros < min1:
        min1 = zeros
        ix1 = layerIx
print(ix1)
print(count(layers[ix1], '1') * count(layers[ix1], '2'))

# part two
image = [['2']*nCols for _ in range(nRows)]

for layer in layers:
    nChanged = 0
    for i in range(nRows):
        row = image[i]
        for j in range(nCols):
            if image[i][j] == '2':
                image[i][j] = layer[i][j]
                nChanged += 1
    if nChanged == 0:
        break

for row in image:
    print(''.join(row).replace('0', ' ').replace('1', '#'))
