import sys, os, time, re, copy, math
from fractions import Fraction as F
from functools import reduce, lru_cache, cmp_to_key
import aoctimer

data = open(os.path.join(sys.path[0], '10.txt')).read().split('\n')

asteroids = {
    (x, y)
    for y in range(len(data))
    for x in range(len(data[0]))
    if data[y][x] == '#'}

def dir_lt(dir1, dir2):
    if dir1[0] != dir2[0]:
        return -1 if dir2[0] else 1
    if dir1[1] == dir2[1]:
        return 0
    if dir1[1] is None or dir2[1] is None:
        return -1 if dir1[1] is None else 1
    return -1 if dir1[1] < dir2[1] else 1

def make_dir(x0, y0, x1, y1):
    return (
        x1<x0 or (x1==x0 and y1>y0), # flag for side; False means right side, or north
        F(0) if x1==x0 else None if y1==y0 else F(y1-y0, x1-x0) # None means neg infinity
    )

# part one
station = None
max_visible = 0
for x0, y0 in asteroids:
    directions = {
        make_dir(x0, y0, x1, y1)
        for [x1, y1] in asteroids
        if [x1, y1] != [x0, y0]
    }
    if len(directions) > max_visible:
        max_visible = len(directions)
        station = [x0, y0]
print(station) # [17, 23]
print(max_visible) # 296

# part two
dir2asteroids = {}
for x, y in asteroids:
    if [x, y] != station:
        dir = make_dir(station[0], station[1], x, y)
        dir2asteroids.setdefault(dir, []).append((x, y))

# sort asteroids in same direction by distance
for asteroids in dir2asteroids.values():
    asteroids.sort(key=cmp_to_key(lambda a, b: a[0]*a[0]+a[1]*a[1] < b[0]*b[0]+b[1]*b[1]))

i = 0
blasted_200th = None
while blasted_200th is None:
    blastdirs = sorted(dir2asteroids.keys(), key=cmp_to_key(dir_lt))
    for dir in blastdirs:
        asteroids = dir2asteroids[dir]
        blasted = asteroids.pop(0)
        #print("blasting:", blasted, "in direction", dir)
        if not asteroids:
            dir2asteroids.pop(dir)
        i += 1
        if i == 200:
            blasted_200th = blasted
            break

print(blasted_200th[0]*100 + blasted_200th[1])
