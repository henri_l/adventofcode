import sys, os, time, re, copy, math
from functools import reduce, lru_cache
import aoctimer
from intcode import Intcode

data = open(os.path.join(sys.path[0], '5.txt')).read()
code = Intcode(data)

print(code.copy().input(1).run())
print(code.copy().input(5).run())
