import sys, os, time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

data = open(os.path.join(sys.path[0], '1.txt')).read()
rows = data.split('\n')
rows = [int(x) for x in rows]

def req(x):
    return x//3 - 2

def b_req(x):
    res = 0
    x = req(x)
    while x > 0:
        res += x
        x = req(x)
    return res

print(sum(map(req, rows)))
print(sum(map(b_req, rows)))
