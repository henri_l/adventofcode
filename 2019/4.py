import sys, os, time, re, copy, math
from functools import reduce, lru_cache
import aoctimer

count = 0
def valid(pw):
    s = str(pw)
    #if all([s[i] != s[i+1] for i in range(5)]):
    if not ( \
       (               s[0]==s[1] and s[1]!=s[2]) or \
       (s[0]!=s[1] and s[1]==s[2] and s[2]!=s[3]) or \
       (s[1]!=s[2] and s[2]==s[3] and s[3]!=s[4]) or \
       (s[2]!=s[3] and s[3]==s[4] and s[4]!=s[5]) or \
       (s[3]!=s[4] and s[4]==s[5]               )):
        return False
    if any([int(s[i]) > int(s[i+1]) for i in range(5)]):
        return False
    return True

print(sum(map(valid, range(284639, 748759+1))))
