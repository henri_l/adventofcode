class Intcode:
    def __init__(self, code, inputs = []):
        if isinstance(code, str):
            self.code = [int(x) for x in code.split(',')]
        else:
            self.code = code
        self.i = 0
        self.relBase = 0
        self.inputs = inputs

    def copy(self):
        res = Intcode(self.code.copy(), self.inputs.copy())
        res.i = self.i
        res.relBase = self.relBase
        return res

    def input(self, x):
        self.inputs.append(x)
        return self

    # return the index that the parameter refers to
    def _param_ix(self, param):
        cmdix = 2 + param
        cmdstr = str(self.code[self.i])
        flag = 0 if len(cmdstr) < cmdix else int(cmdstr[-cmdix])
        if flag == 0: # position mode
            return self.code[self.i + param]
        elif flag == 1: # immediate mode
            return self.i + param
        elif flag == 2: # relative mode
            return self.code[self.i + param] + self.relBase
        else:
            raise RuntimeError('unknown flag: {}'.format(flag))

    # return list of ixs that params refer to
    def _read_param_ixs(self, n):
        ixs = tuple(self._param_ix(x+1) for x in range(n))
        max_ix = max(ixs)
        if max_ix >= len(self.code):
            self.code += [0]*(max_ix-len(self.code)+1)
        return ixs if len(ixs)>1 else ixs[0]

    # run main code.
    # breakOnOutput=n breaks when n results have been returned, 0 runs until end
    def run(self, breakOnOutput = 0):
        outputs = []
        while True:
            cmd = self.code[self.i] % 100
            if cmd == 99:
                break
            elif cmd == 1 or cmd == 2:
                ix1, ix2, ix3 = self._read_param_ixs(3)
                if cmd == 1:
                    self.code[ix3] = self.code[ix1]+self.code[ix2]
                else:
                    self.code[ix3] = self.code[ix1]*self.code[ix2]
                self.i += 4
            elif cmd == 3:
                if not self.inputs:
                    raise RuntimeError('no inputs')
                ix1 = self._read_param_ixs(1)
                self.code[ix1] = self.inputs.pop(0)
                self.i += 2
            elif cmd == 4:
                ix1 = self._read_param_ixs(1)
                outputs.append(self.code[ix1])
                self.i += 2
                if breakOnOutput and len(outputs) >= breakOnOutput:
                    return outputs
            elif cmd == 5 or cmd == 6:
                ix1, ix2 = self._read_param_ixs(2)
                if (cmd == 5) == (self.code[ix1] != 0):
                    self.i = self.code[ix2]
                else:
                    self.i += 3
            elif cmd == 7 or cmd == 8:
                ix1, ix2, ix3 = self._read_param_ixs(3)
                self.code[ix3] = int(
                    (cmd == 7 and self.code[ix1] < self.code[ix2])
                    or (cmd == 8 and self.code[ix1] == self.code[ix2]))
                self.i += 4
            elif cmd == 9:
                ix1 = self._read_param_ixs(1)
                self.relBase += self.code[ix1]
                self.i += 2
            else:
                raise RuntimeError('bad code at pos {}: {}'.format(self.i, self.code[self.i]))
        return outputs
