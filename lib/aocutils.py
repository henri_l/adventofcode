import math, operator

class Point(tuple):
    def __new__(cls, *args):
        return super().__new__(cls, (*args,))
    def __add__(self, rhs):
        return Point(*map(operator.add, self, rhs))
    def __sub__(self, rhs):
        return Point(*map(operator.sub, self, rhs))


# print 2-dim sparse coordinates, set or dict. x grows right, y grows down
def printSparse(data, val_fn = lambda x : str(x)):
    x_min = y_min = math.inf
    x_max = y_max = -math.inf
    for x, y in data if isinstance(data, set) else data.keys():
        x_min = min(x_min, x)
        y_min = min(y_min, y)
        x_max = max(x_max, x)
        y_max = max(y_max, y)
    for y in range(y_min, y_max+1):
        row = ''
        for x in range(x_min, x_max+1):
            if Point(x, y) in data:
                row += '#' if isinstance(data, set) else val_fn(data[Point(x, y)])
            else:
                row += ' '
        print(row)
    print()


# least common multiple
def lcm(a, b):
    return abs(a*b)//math.gcd(a, b)
