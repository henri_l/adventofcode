import time, atexit

def endlog():
    end = time.perf_counter()
    print('Script took {:f} ms'.format((end-start)*1000))

start = time.perf_counter()
atexit.register(endlog)
