import aoctimer

data = open('2021/3.txt').read()
rows = data.split('\n')

nums = [int(x, 2) for x in rows]

length = len(rows[0])
bits = [2**x for x in range(length-1, -1, -1)]

oneness = [0]*length # ones - zeros
for num in nums:
    for i, bit in enumerate(bits):
        oneness[i] += 1 if num & bit else -1

gamma = epsilon = 0
for one, bit in zip(oneness, bits):
    if one >= 0:
        epsilon += bit
    else:
        gamma += bit
print(epsilon*gamma)

def rating(criteria):
    filtered = nums.copy()
    for bit in bits:
        if len(filtered) <= 1:
            break
        oneness = sum(1 if num & bit else -1 for num in filtered)
        keep = criteria == (oneness >= 0)
        filtered = [x for x in filtered if bool(x & bit) == keep]
    return filtered[0]
oxygen = rating(1)
scrubber = rating(0)
print(oxygen*scrubber)
