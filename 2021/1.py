import aoctimer

data = open('2021/1.txt').read()
rows = data.split('\n')

depths = [int(x) for x in rows]

inc = 0
for i in range(1, len(depths)):
    if depths[i] > depths[i-1]:
        inc += 1
print(inc)

inc = 0
for i in range(3, len(depths)):
    if depths[i] > depths[i-3]:
        inc += 1
print(inc)
