import aoctimer

data = open('2021/10.txt').read()
lines = data.split('\n')

brackets = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>',
}

pointMap = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137,
}

def check(line, incomplete):
    stack = []
    for c in line:
        if c in brackets.keys():
            stack.append(c)
        elif not stack or brackets[stack.pop()] != c:
            return pointMap[c]
    incomplete.append(stack)
    return 0

incomplete = []
print(sum(check(line, incomplete) for line in lines))

scoreMap = {
    '(': 1,
    '[': 2,
    '{': 3,
    '<': 4,
}

def score(stack):
    res = 0
    for c in reversed(stack):
        res *= 5
        res += scoreMap[c]
    return res

scores = sorted(score(x) for x in incomplete)
print(scores[len(scores)//2])
