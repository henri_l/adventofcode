from aocutils import Point
import aoctimer

data = open('2021/15.txt').read()
rows = data.split('\n')
risks = dict()
for y, row in enumerate(rows):
    for x, risk in enumerate(row):
        risks[Point(x, y)] = int(risk)

def findpath(risks):
    totalrisks = {Point(0, 0): 0}
    edge = {Point(0, 0)}
    while edge:
        newedge = set()
        for cur in edge:
            for dp in (Point(1, 0), Point(-1, 0), Point(0, 1), Point(0, -1)):
                neighbour = cur + dp
                if neighbour in risks:
                    nrisk = totalrisks[cur] + risks[neighbour]
                    if neighbour not in totalrisks or totalrisks[neighbour] > nrisk:
                        totalrisks[neighbour] = nrisk
                        newedge.add(neighbour)
        edge = newedge
    return totalrisks[max(risks.keys())]

print(findpath(risks))

nrows = len(rows)
ncols = len(rows[0])
risks2 = {
    Point(
        xblock*ncols + x,
        yblock*nrows + y )
    : ((risks[Point(x, y)] - 1 + xblock + yblock) % 9) + 1
    for yblock in range(5)
    for xblock in range(5)
    for y in range(nrows)
    for x in range(ncols)
}

# slow
print(findpath(risks2))
