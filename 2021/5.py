import re
import aoctimer

data = open('2021/5.txt').read()
rows = data.split('\n')

lines = []
for row in rows:
    [(x0, y0, x1, y1)] = re.findall('(.*),(.*) -> (.*),(.*)', row)
    lines.append([(int(x0), int(y0)), (int(x1), int(y1))])

side = 1000

def makeGrid(skipDiag):
    grid = [[0]*side for _ in range(side)]
    for [(x0,y0), (x1,y1)] in lines:
        dx = x1 - x0
        dy = y1 - y0
        if skipDiag and dx != 0 and dy != 0:
            continue
        nSteps = max(abs(dx), abs(dy))
        for k in range(nSteps + 1):
            x = x0 + k*int(dx/nSteps)
            y = y0 + k*int(dy/nSteps)
            grid[y][x] += 1
    return grid

def countIntersect(grid):
    return sum(sum(1 for x in row if x > 1) for row in grid)

print(countIntersect(makeGrid(True)))
print(countIntersect(makeGrid(False)))
