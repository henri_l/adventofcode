import aoctimer

digits = {
    'abcefg':   0,
    'cf':       1,
    'acdeg':    2,
    'acdfg':    3,
    'bcdf':     4,
    'abdfg':    5,
    'abdefg':   6,
    'acf':      7,
    'abcdefg':  8,
    'abcdfg':   9,
}

data = open('2021/8.txt').read()

patterns = []
for row in data.split('\n'):
    key, code = row.split(' | ')
    patterns.append((key.split(' '), code.split(' ')))

print(sum(len([x for x in p[1] if len(x) in {2, 3, 4, 7}]) for p in patterns))

def sortedStr(x):
    return ''.join(sorted(x))

def check(mapping, pattern):
    key, code = pattern
    mapped = dict()
    for k in key:
        real = sortedStr(mapping[c] for c in k)
        if real not in digits:
            return None
        num = digits[real]
        mapped[sortedStr(k)] = num
    res = int(''.join(str(mapped[sortedStr(x)]) for x in code))
    return res

def recursiveFind(freeSrc, freeTgt, mapping, pattern):
    if not freeSrc:
        return check(mapping, pattern)
    nextFreeSrc = freeSrc.copy()
    src = nextFreeSrc.pop()
    for tgt in freeTgt:
        nextFreeTgt = freeTgt.difference({tgt})
        mapping[src] = tgt
        res = recursiveFind(nextFreeSrc, nextFreeTgt, mapping, pattern)
        if res:
            return res
    return None

print(sum(recursiveFind(set('abcdefg'), set('abcdefg'), dict(), p) for p in patterns))
