from collections import Counter
from functools import lru_cache
import aoctimer

data = open('2021/21.txt').read()
positions = tuple(int(row[-1]) - 1 for row in data.split('\n'))

nrolls = 0
def simpledice():
    global nrolls
    res = 3 + sum((nrolls + i) % 100 for i in range(3))
    nrolls += 3
    return ((1, res),)

def quantumdice():
    return ( (1, 3), (3, 4), (6, 5), (7, 6), (6, 7), (3, 8), (1, 9), )

@lru_cache(None)
def winners(player, positions, scores, limit, dice):
    res = Counter()
    for n, val in dice():
        p = (positions[player] + val) % 10
        newpos = (positions[0], p) if player else (p, positions[1])
        s = scores[player] + p + 1
        newscores = (scores[0], s) if player else (s, scores[1])
        if s >= limit:
            tmp = Counter({player: 1})
            if limit == 1000: # part 1
                global nrolls
                print(scores[1-player] * nrolls)
        else:
            tmp = winners(1-player, newpos, newscores, limit, dice)
        for _ in range(n):
            res += tmp
    return res

winners(0, positions, (0, 0), 1000, simpledice)

wincounts = winners(0, positions, (0, 0), 21, quantumdice)
print(max(wincounts.values()))
