from aocutils import Point
import aoctimer

rows = open('2021/25.txt').read().split('\n')

cucumbers = {
    Point(x, y): c
    for y, row in enumerate(rows)
    for x, c in enumerate(row)
    if c in '>v'
}

dims = Point(*(max(p[i] for p in cucumbers) + 1 for i in (0, 1)))

herds = (
    ('>', Point(1, 0)),
    ('v', Point(0, 1)),
)

def loop(p):
    return Point(*(p[i] % dims[i] for i in range(len(p))))

step = 0
changed = True
while changed:
    step += 1
    changed = False
    for active, dp in herds:
        cucumbers2 = cucumbers.copy()
        for p0, c in cucumbers.items():
            if c == active:
                p1 = loop(p0 + dp)
                if p1 not in cucumbers:
                    cucumbers2[p1] = cucumbers2.pop(p0)
                    changed = True
        cucumbers = cucumbers2

print(step)
