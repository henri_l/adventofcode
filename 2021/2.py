import aoctimer

data = open('2021/2.txt').read()
rows = data.split('\n')

steps = [(x[0], int(x[1])) for x in [y.split(' ') for y in rows]]

pos = depth = 0
for (dir, dist) in steps:
    if dir == 'forward': pos += dist
    elif dir == 'down': depth += dist
    elif dir == 'up': depth -= dist
    else: raise RuntimeError('unknown dir: ' + dir)
print(pos*depth)

pos = depth = aim = 0
for (dir, dist) in steps:
    if dir == 'forward':
        pos += dist
        depth += aim*dist
    elif dir == 'down':
        aim += dist
    elif dir == 'up':
        aim -= dist
    else: raise RuntimeError('unknown dir: ' + dir)
print(pos*depth)
