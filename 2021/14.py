from collections import defaultdict
import aoctimer

data = open('2021/14.txt').read()
template, rules = data.split('\n\n')
rules = {pair: elem for [pair, elem] in [row.split(' -> ') for row in rules.split('\n')]}

initPairs = defaultdict(int)
for i in range(0, len(template)-1):
    initPairs[template[i:i+2]] += 1

def polymerize(cycles):
    pairs = initPairs.copy()
    for _ in range(cycles):
        tmp = defaultdict(int)
        for p, n in pairs.items():
            c = rules[p]
            tmp[p[0]+c] += n
            tmp[c+p[1]] += n
        pairs = tmp

    counts = defaultdict(int)
    counts[template[0]] = 1
    for p, n in pairs.items():
        counts[p[1]] += n

    return max(counts.values()) - min(counts.values())

print(polymerize(10))
print(polymerize(40))
