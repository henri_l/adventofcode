from collections import defaultdict
import aoctimer

data = open('2021/12.txt').read()

caves = defaultdict(set)
for row in data.split('\n'):
    [a, b] = row.split('-')
    caves[a].add(b)
    caves[b].add(a)

def nPaths(start, visited, extra):
    res = 0
    for next in caves[start]:
        if next == 'end':
            res += 1
        elif next != 'start':
            if next.isupper():
                res += nPaths(next, visited, extra)
            elif next not in visited:
                res += nPaths(next, visited.union({next}), extra)
            elif extra:
                res += nPaths(next, visited, False)
    return res

print(nPaths('start', set(), False))
print(nPaths('start', set(), True))
