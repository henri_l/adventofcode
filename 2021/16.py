import math
import aoctimer

data = open('2021/16.txt').read()

totalversion = 0

def hex2bin(hex):
    b = bin(int(hex, 16))[2:]
    return '0'*(4*len(hex)-len(b)) + b

def fn(typeid, params):
    match typeid:
        case 0: return sum(params)
        case 1: return math.prod(params)
        case 2: return min(params)
        case 3: return max(params)
        case 5: return int(params[0] > params[1])
        case 6: return int(params[0] < params[1])
        case 7: return int(params[0] == params[1])

def parse(packet):
    version = int(packet[:3], 2)
    typeid = int(packet[3:6], 2)
    payload = packet[6:]

    global totalversion
    totalversion += version

    if typeid == 4:
        valbin = ''
        while True:
            valbin += payload[1:5]
            if payload[0] == '0':
                return int(valbin, 2), payload[5:]
            payload = payload[5:]
    else:
        lenid = payload[0]
        params = []
        if lenid == '0':
            sublen = int(payload[1:16], 2)
            sub = payload[16:16+sublen]
            rest = payload[16+sublen:]
            while sub:
                val, sub = parse(sub)
                params.append(val)
        else:
            nsub = int(payload[1:12], 2)
            rest = payload[12:]
            for _ in range(nsub):
                val, rest = parse(rest)
                params.append(val)
        return fn(typeid, params), rest

val, _ = parse(hex2bin(data))
print(totalversion)
print(val)
