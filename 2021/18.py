from functools import reduce
import aoctimer

data = open('2021/18.txt').read()

class Num:
    def __init__(self, val, parent):
        self.val = val
        self.parent = parent

    def __str__(self):
        if self.isnum():
            return str(self.val)
        return '[{},{}]'.format(*self.val)

    def clone(self):
        if self.isnum():
            return Num(self.val, None)
        a = self.val[0].clone()
        b = self.val[1].clone()
        root = Num((a, b), None)
        a.parent = b.parent = root
        return root

    def isnum(self):
        return isinstance(self.val, int)

    def splitone(self):
        if self.isnum():
            if self.val > 9:
                self.val = (Num(self.val//2, self), Num(self.val - self.val//2, self))
                return True
            return False
        return self.val[0].splitone() or self.val[1].splitone()

    def neighbour(self, side):
        if not self.parent:
            return None
        if self.parent.val[side] is self:
            return self.parent.neighbour(side)
        return self.parent.val[side]

    def edge(self, side):
        if self.isnum():
            return self
        return self.val[side].edge(side)

    def findDeepPair(self, depth):
        if self.isnum():
            return None
        if depth == 0:
            return self
        return self.val[0].findDeepPair(depth - 1) or self.val[1].findDeepPair(depth - 1)

    def explodeone(self):
        deepPair = self.findDeepPair(4)
        if deepPair:
            for side in (0, 1):
                neighbour = deepPair.neighbour(side)
                if neighbour:
                    neighbour.edge(1-side).val += deepPair.val[side].val
            zero = Num(0, deepPair.parent)
            if deepPair.parent.val[0] == deepPair:
                deepPair.parent.val = (zero, deepPair.parent.val[1])
            else:
                deepPair.parent.val = (deepPair.parent.val[0], zero)
            return True
        return False

    def reduce(self):
        while self.explodeone() or self.splitone():
            continue

    def magnitude(self):
        if self.isnum():
            return self.val
        return 3*self.val[0].magnitude() + 2*self.val[1].magnitude()

def parse(s, parent):
    if s[0].isnumeric():
        return Num(int(s[0]), parent), s[1:] # assume reduced form
    left, rest = parse(s[1:], parent) # assume rest[0] == ','
    right, rest = parse(rest[1:], parent) # assume rest[0] == ']'
    return Num((left, right), parent), rest[1:]

nums = [parse(line, None)[0] for line in data.split('\n')]

def add(a, b):
    res = Num((a.clone(), b.clone()), None)
    res.val[0].parent = res.val[1].parent = res
    res.reduce()
    return res

print(reduce(add, nums).magnitude())

maxMagnitude = 0
for i in range(len(nums)):
    for j in range(len(nums)):
        if i != j:
            maxMagnitude = max(maxMagnitude, add(nums[i], nums[j]).magnitude())
            maxMagnitude = max(maxMagnitude, add(nums[j], nums[i]).magnitude())
print(maxMagnitude)
