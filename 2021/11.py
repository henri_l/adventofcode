import aoctimer
from aocutils import Point

data = open('2021/11.txt').read()

grid = dict()
for y, row in enumerate(data.split('\n')):
    for x, e in enumerate(row):
        grid[Point(x, y)] = int(e)
print(grid)

neighbours = [Point(x, y) for x in (-1, 0, 1) for y in (-1, 0, 1) if x != 0 or y != 0]

def flash():
    res = 0
    new = 0
    while True:
        for p, e in grid.items():
            if e > 9:
                grid[p] = 0
                for dp in neighbours:
                    n = p + dp
                    if n in grid and grid[n] > 0:
                        grid[n] += 1
                new += 1
        if new > 0:
            res += new
            new = 0
        else:
            return res

totalFlashes = 0
step = 1
while True:
    for p in grid:
        grid[p] += 1
    flashes = flash()
    totalFlashes += flashes
    if step == 100:
        print(totalFlashes)
    if flashes == len(grid):
        print(step)
        break
    step += 1
