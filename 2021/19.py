import numpy as np
import aoctimer

data = open('2021/19.txt').read()
scanners = [{tuple(map(int, r.split(','))) for r in g.split('\n')[1:]} for g in data.split('\n\n')]

id = np.identity(3)
xrot = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])
yrot = np.array([[0, 0, 1], [0, 1, 0], [-1, 0, 0]])
zrot = np.array([[0, -1, 0], [1, 0, 0], [0, 0, 1]])

zrotations = ( id, zrot, zrot.dot(zrot), zrot.dot(zrot).dot(zrot), )
axisrotations = ( id, xrot, yrot, xrot.dot(xrot), xrot.dot(zrot), xrot.dot(zrot).dot(zrot), )
allrotations = tuple(zrotations[i%4].dot(axisrotations[i//4]) for i in range(24))

def rotations(scanner):
    npPoints = np.array(list(scanner))
    return [{tuple(map(round, a.flatten())) for a in npPoints.dot(r)} for r in allrotations]

def pointsum(a, b):
    return tuple(a[i] + b[i] for i in range(len(a)))

matchCondition = lambda a, b: len(a.intersection(b)) >= 12

def matchScanners(found, free, foundpos):
    for i in range(len(free)):
        for rotated in rotations(free[i]):
            for existing in found:
                checkedpositions = set()
                for newroot in rotated:
                    for oldroot in existing:
                        scannerpos = pointsum(oldroot, tuple(-x for x in newroot))
                        if scannerpos in checkedpositions:
                            continue
                        checkedpositions.add(scannerpos)
                        shifted = {pointsum(scannerpos, x) for x in rotated}
                        if matchCondition(shifted, existing):
                            #print('found beacon')
                            newFound = found + [shifted]
                            newFree = free[:i] + free[i+1:]
                            foundpos.add(scannerpos)
                            if not newFree:
                                return newFound, foundpos
                            return matchScanners(newFound, newFree, foundpos)

# really slow - could optimize a lot by considering scanner areas and not only points
beaconpos, scannerpos = matchScanners(scanners[:1], scanners[1:], {(0, 0, 0)})
print(len(set().union(*beaconpos)))

def distance(pos1, pos2):
    return sum(abs(a - b) for a, b in zip(pos1, pos2))

print(max(distance(a, b) for a in scannerpos for b in scannerpos))
