from functools import lru_cache
import aoctimer

rows = open('2021/24.txt').read().split('\n')
instructions = []
for row in rows:
    inst = row.split(' ')
    if inst[0] == 'inp':
        instructions.append(inst[1])
    else:
        match inst[0]:
            case 'add':
                fn = lambda a, b: a + b
            case 'mul':
                fn = lambda a, b: a * b
            case 'div':
                fn = lambda a, b: a // b
            case 'mod':
                fn = lambda a, b: a % b
            case 'eql':
                fn = lambda a, b: int(a == b)
        instructions.append((fn, inst[1], inst[2]))

def calcStep(x, ix):
    x = list(x)
    while ix < len(instructions) and not isinstance(instructions[ix], str):
        instr = instructions[ix]
        ix1 = 'wxyz'.index(instr[1])
        p2 = instr[2]
        val = x['wxyz'.index(p2)] if p2 in 'wxyz' else int(p2)
        x[ix1] = instr[0](x[ix1], val)
        ix += 1
    return tuple(x), ix

@lru_cache(None)
def recFind(x, ix, smallest):
    while ix < len(instructions):
        instr = instructions[ix]
        if isinstance(instr, str):
            varIx = 'wxyz'.index(instr)
            for k in (range(1, 10) if smallest else range(9, 0, -1)):
                x = (*x[:varIx], k, *x[varIx+1:])
                found = recFind(x, ix+1, smallest)
                if isinstance(found, str):
                    return str(k) + found
            return None
        else:
            x, ix = calcStep(x, ix)
    if x[3] == 0:
        print(x)
        return ''
    return None

# part 1 takes about 30 sec
print(recFind((0, 0, 0, 0), 0, False))
# part 2 takes about an hour
print(recFind((0, 0, 0, 0), 0, True))
