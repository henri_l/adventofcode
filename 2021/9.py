from collections import defaultdict
import aoctimer

data = open('2021/9.txt').read()

grid = [[int(x) for x in row] for row in data.split('\n')]

rows, cols = len(grid), len(grid[0])

def tryGet(r, c):
    if r < 0 or c < 0 or r >= rows or c >= cols:
        return None
    return grid[r][c]

def risk(r, c):
    height = grid[r][c]
    for dr, dc in ((-1,0), (1,0), (0,-1), (0,1)):
        neighbour = tryGet(r+dr, c+dc)
        if neighbour is not None and neighbour <= height:
            return 0
    return 1 + height

print(sum(risk(r, c) for r in range(rows) for c in range(cols)))

basins = defaultdict(set)
for r in range(rows):
    for c in range(cols):
        height = grid[r][c]
        if height < 9:
            x, y = r, c
            while risk(x, y) == 0:
                for dx, dy in ((-1,0), (1,0), (0,-1), (0,1)):
                    neighbour = tryGet(x+dx, y+dy)
                    if neighbour is not None and neighbour < height:
                        x, y = x+dx, y+dy
                        height = neighbour
                        continue
            basins[(x,y)].add((r, c))

bs = sorted(len(x) for x in basins.values())
print(bs[-3]*bs[-2]*bs[-1])
