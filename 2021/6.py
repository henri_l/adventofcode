from collections import defaultdict
import aoctimer

data = open('2021/6.txt').read()

ages = defaultdict(int)
for age in data.split(','):
    ages[int(age)] += 1

def populationAfter(init, t):
    ages = init.copy()
    for _ in range(t):
        newages = defaultdict(int)
        for age, n in ages.items():
            if age == 0:
                newages[6] += n
                newages[8] += n
            else:
                newages[age-1] += n
        ages = newages
    return sum(ages.values())

print(populationAfter(ages, 80))
print(populationAfter(ages, 256))
