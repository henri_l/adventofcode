import re
import aoctimer

data = open('2021/22.txt').read()
steps = []
for row in data.split('\n'):
    [(toggle, x0, x1, y0, y1, z0, z1)] = \
        re.findall('(on|off) x=(.*)\.\.(.*),y=(.*)\.\.(.*),z=(.*)\.\.(.*)', row)
    area = tuple((int(a), int(b) + 1) for a, b in ((x0, x1), (y0, y1), (z0, z1)))
    steps.append((toggle == 'on', area))

def sliceIntersecting(a, r):
    if a[0][0] < r[0][0]:
        yield ((a[0][0], r[0][0]), *a[1:])
    if r[0][1] < a[0][1]:
        yield ((r[0][1], a[0][1]), *a[1:])
    if len(a) > 1:
        for rest in sliceIntersecting(a[1:], r[1:]):
            yield ((max(a[0][0], r[0][0]), min(a[0][1], r[0][1])), *rest)

def slice(area, exclude):
    if any(x[1] < y[0] or y[1] < x[0] for x, y in zip(area, exclude)):
        yield area
    else:
        yield from sliceIntersecting(area, exclude)

def dist(a, b):
    return max(0, b - a)

def initDist(a, b):
    return max(0, min(51, b) - max(-50, a))

initRes = 0
bootRes = 0
checkedAreas = []
for toggle, cur in reversed(steps):
    if toggle:
        slices = [cur]
        for exclude in checkedAreas:
            nextSlices = []
            for s in slices:
                nextSlices.extend(slice(s, exclude))
            slices = nextSlices
        for s in slices:
            initRes += initDist(*s[0]) * initDist(*s[1]) * initDist(*s[2])
            bootRes += dist(*s[0]) * dist(*s[1]) * dist(*s[2])
    checkedAreas.append(cur)

print(initRes)
print(bootRes)
