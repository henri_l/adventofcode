import re
import aoctimer

data = open('2021/17.txt').read()
nums = re.findall('target area: x=(.*)\.\.(.*), y=(.*)\.\.(.*)', data)
[x0, x1, y0, y1] = [int(k) for k in nums[0]]

print(y0 * (y0 + 1) // 2)

nTrajectories = 0
for vx0 in range(1, x1+1):
    for vy0 in range(y0, -y0):
        x, y, vx, vy = 0, 0, vx0, vy0
        advance = lambda: (x + vx, y + vy, max(0, vx - 1), vy - 1)
        while x < x0 and y > y1:
            x, y, vx, vy = advance()
        while x <= x1 and y >= y0:
            if x0 <= x <= x1 and y0 <= y <= y1:
                nTrajectories += 1
                break
            x, y, vx, vy = advance()
print(nTrajectories)
