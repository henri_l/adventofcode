import re
from aocutils import Point
import aoctimer

data = open('2021/13.txt').read()

[coordsStr, foldsStr] = data.split('\n\n')

coords = set()
for row in coordsStr.split('\n'):
    [x, y] = row.split(',')
    coords.add(Point(int(x), int(y)))

folds = []
for row in foldsStr.split('\n'):
    [(dim, pos)] = re.findall('fold along (x|y)=(.*)', row)
    folds.append((0 if dim=='x' else 1, int(pos)))

def foldPoint(p, dim, pos):
    folded = pos-abs(pos-p[dim])
    return Point(*(p[:dim] + (folded,) + p[dim+1:]))

for dim, pos in folds:
    coords = {foldPoint(p, dim, pos) for p in coords}
    print(len(coords))

for y in range(max(p[1] for p in coords)+1):
    row = ''
    for x in range(max(p[0] for p in coords)+1):
        row += '#' if Point(x, y) in coords else ' '
    print(row)
