import aoctimer

data = open('2021/4.txt').read()
blobs = data.split('\n\n')

nums = [int(x) for x in blobs[0].split(',')]
boards = [[[[int(x), False] for x in row.split(' ')] for row in b.split('\n')] for b in blobs[1:]]

def checkWin(board):
    return board and ( \
        any(all(board[r][c][1] for c in range(5)) for r in range(5)) or \
        any(all(board[r][c][1] for r in range(5)) for c in range(5)) )

def score(board, lastNum):
    return lastNum * sum(sum(x[0] for x in row if not x[1]) for row in board)

for num in nums:
    for board in boards:
        for row in board:
            for item in row:
                if item[0] == num:
                    item[1] = True
    for i in range(len(boards)):
        board = boards[i]
        if checkWin(board):
            if all(boards):
                print('first:', score(board, num))
            elif sum(bool(b) for b in boards) == 1:
                print('last:', score(board, num))
            boards[i] = []
