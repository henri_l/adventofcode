from aocutils import Point #, printSparse
import aoctimer

data = open('2021/20.txt').read()
algo, imgstr = data.split('\n\n')
img = {
    Point(x, y)
    for y, row in enumerate(imgstr.split('\n'))
    for x, c in enumerate(row)
    if c == '#'
}

def bounds(pts):
    lower = upper = None
    for p in pts:
        lower = p if lower is None else Point(*(min(a,b) for a,b in zip(lower, p)))
        upper = p if upper is None else Point(*(max(a,b) for a,b in zip(upper, p)))
    return lower, upper

def lit(x, y, img, lower, upper, default):
    if lower[0] <= x <= upper[0] and lower[1] <= y <= upper[1]:
        return Point(x, y) in img
    return default

def binkey(x, y, img, lower, upper, default):
    return ''.join(str(int(lit(x+dx, y+dy, img, lower, upper, default)))
                   for dy in (-1,0,1) for dx in (-1,0,1))

def newvalue(binkey):
    return algo[int(binkey, 2)] == '#'

#printSparse(img)
def enhance(img, n):
    default = False
    for _ in range(n):
        lower, upper = bounds(img)
        img = {
            Point(x, y)
            for y in range(lower[1]-1, upper[1]+2)
            for x in range(lower[0]-1, upper[0]+2)
            if newvalue(binkey(x, y, img, lower, upper, default))
        }
        default = newvalue('11111111' if default else '00000000')
        #printSparse(img)
    return img

print(len(enhance(img, 2)))
print(len(enhance(img, 50)))
