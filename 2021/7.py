import aoctimer

data = open('2021/7.txt').read()

crabs = sorted([int(x) for x in data.split(',')])

align = crabs[len(crabs)//2]
print(sum(abs(x-align) for x in crabs))

def consumption(a, b):
    dist = abs(a-b)
    return (1+dist)*dist//2

def cost(align):
    return sum(consumption(x, align) for x in crabs)

print(min(cost(align) for align in range(crabs[0], crabs[-1]+1)))
