from aocutils import Point
import aoctimer

rows = open('2021/23.txt').read().split('\n')

costs = {'A': 1, 'B': 10, 'C': 100, 'D': 1000}
targetcols = {'A': 3, 'B': 5, 'C': 7, 'D': 9}

for partTwo in (False, True):
    if partTwo:
        rows.insert(3, '  #D#C#B#A#')
        rows.insert(4, '  #D#B#A#C#')

    allnodes = set()
    hallways = set()
    allrooms = set()
    amphipods = dict()
    for y, row in enumerate(rows):
        for x, c in enumerate(row):
            p = Point(x, y)
            if c in '.ABCD':
                allnodes.add(p)
            if c in 'ABCD':
                allrooms.add(p)
                amphipods[p] = c

    yMax = max(p[1] for p in allrooms)
    targetrooms = {c: tuple(Point(x, y) for y in range(2, yMax+1)) for c, x in targetcols.items()}

    nds = (Point(-1, 0), Point(1, 0), Point(0, -1), Point(0, 1))
    intersections = set(p for p in allnodes if sum(p+d in allnodes for d in nds) > 2)
    stopnodes = allnodes.difference(intersections)

    def getRoutes(start, visited):
        res = []
        for d in nds:
            go = start + d
            if go in allnodes and go not in visited:
                if go not in intersections:
                    res.append((go,))
                for r in getRoutes(go, visited.union({go})):
                    res.append((go, *r))
        return res

    # start point to set of possible routes
    routes = {s: getRoutes(s, {s}) for s in stopnodes}
    outroutes = {s: [r for r in routes[s] if r[-1] not in allrooms] for s in allrooms}
    # (amphipod char, start point) -> in routes
    inroutes = {
        (c, start): [r for r in routes[start] if r[-1] in allrooms and r[-1][0] == targetcols[c]]
        for c in 'ABCD'
        for start in stopnodes
    }

    def isFreeRoute(route, amphipods):
        for p in route:
            if p in amphipods:
                return False
        return True

    # assumes route is free and target point matches c
    def canMoveIn(c, start, inroute, amphipods):
        x = targetcols[c]
        for y in range(inroute[-1][1]+1, yMax+1):
            below = Point(x, y)
            if below not in amphipods or amphipods[below] != c or start == below:
                return False
        return True

    def routecost(route, c):
        return costs[c] * len(route)

    def minCost(amphipods, moved, finished):
        # 1) move all in (cost is same regardless of move in order)
        allMovedIn = False
        moveInCost = 0
        #printSparse(amphipods)
        while not allMovedIn:
            allMovedIn = True
            for startpos, c in amphipods.items():
                if startpos not in finished:
                    for r in inroutes[c, startpos]:
                        if isFreeRoute(r, amphipods) and canMoveIn(c, startpos, r, amphipods):
                            amphipods[r[-1]] = amphipods.pop(startpos)
                            moved.discard(startpos)
                            moved.add(r[-1])
                            finished.discard(startpos)
                            finished.add(r[-1])
                            moveInCost += routecost(r, c)
                            allMovedIn = False
                            break
                if not allMovedIn:
                    break
        if len(finished) == len(amphipods):
            return moveInCost

        # 2) move out
        minimum = None
        for startpos, c in amphipods.items():
            if startpos not in moved and startpos not in finished:
                for r in outroutes[startpos]:
                    if isFreeRoute(r, amphipods):
                        amphipods2 = amphipods.copy()
                        amphipods2[r[-1]] = amphipods2.pop(startpos)
                        moved2 = moved.copy()
                        moved2.discard(startpos)
                        moved2.add(r[-1])
                        nextCost = minCost(amphipods2, moved2, finished.copy())
                        if nextCost is not None:
                            total = moveInCost + routecost(r, c) + nextCost
                            minimum = min(minimum or total, total)
        return minimum

    print(minCost(amphipods, set(), set()))
